/*
 * HTML5 Sortable jQuery Plugin
 * Modified and partially rewriten for Avrora OS
 * http://farhadi.ir/projects/html5sortable
 *
 * Copyright 2012, Ali Farhadi
 * Released under the MIT license.
 */
(function($) {
    var dragging,
        placeholders = $();

    $.fn.sortable = function(options) {
        var method = String(options);
        options = $.extend({
            connectWith: false,
            uniqueIdAttr: 'id',
            interactWith: []
        }, options);

        return this.each(function() {
            var items;

            if (/^enable|disable|destroy$/.test(method)) {
                items = $(this).children($(this).data('items')).attr('draggable', method === 'enable');
                if (method === 'destroy') {
                    items
                        .add(this)
                        .removeData('connectWith items')
                        .off('dragstart dragend selectstart dragover dragenter drop');
                }
                return;
            }

            var isHandle,
                index,
                placeholder = $('<' + (/^ul|ol$/i.test(this.tagName) ? 'li' : 'div') + ' class="sortable-placeholder">');

            items = $(this).children(options.items);
            items
                .find(options.handle)
                .mousedown(function() {
                    isHandle = true;
                })
                .mouseup(function() {
                    isHandle = false;
                });

            $(this).data('items', options.items);

            placeholder.css({ 'width' : $(this).find('li').width(), 'height' : $(this).find('li').height() });

            placeholders = placeholders.add(placeholder);
            if (options.connectWith) {
                $(options.connectWith)
                    .add(this)
                    .data('connectWith', options.connectWith);
            }

            if (options.interactWith.length) {
                _.each(options.interactWith, function(interactWith) {
                    interactWith.element
                        .on('dragenter', function() {
                            if (interactWith.onDragenter) {
                                interactWith.onDragenter(interactWith.element);
                            }
                            return false;
                        })
                        .on('dragleave', function() {
                            if (interactWith.onDragleave) {
                                interactWith.onDragleave(interactWith.element);
                            }
                        })
                        .on('dragover', function(e) {
                            e.preventDefault();
                            e.originalEvent.dataTransfer.dropEffect = 'copy';

                            if (interactWith.onDragover) {
                                interactWith.onDragover(interactWith.element);
                            }
                        })
                        .on('drop', function(e) {
                            e.stopPropagation();
                            var elId = e.originalEvent.dataTransfer.getData('ElementID');

                            if (interactWith.onDrop) {
                                interactWith.onDrop(elId, interactWith.element);
                            }
                        });
                });
            }

            items
                .attr('draggable', 'true')
                .on('dragstart', function(e) {
                    if (options.handle && !isHandle) {
                        return false;
                    }
                    isHandle = false;
                    e.originalEvent.dataTransfer.effectAllowed = 'copy';

                    var elementId = '';
                    if ($(this).attr(options.uniqueIdAttr)) elementId = $(this).attr(options.uniqueIdAttr);
                    else if ($(this).find('[' + options.uniqueIdAttr + ']').length) elementId = $(this).find('[' + options.uniqueIdAttr + ']').attr(options.uniqueIdAttr);

                    e.originalEvent.dataTransfer.setData('ElementID', elementId);
                    index = (dragging = $(this)).addClass('sortable-dragging').index();
                })
                .on('dragend', function() {
                    if (!dragging) {
                        return;
                    }
                    dragging.removeClass('sortable-dragging').show();
                    placeholders.detach();
                    if (index !== dragging.index()) {
                        dragging.parent().trigger('sortupdate', {item: dragging});
                    }
                    dragging = null;
                })
                .add([this, placeholder])
                .on('dragover dragenter drop', function(e) {
                    if (!items.is(dragging) && options.connectWith !== $(dragging).parent().data('connectWith')) {
                        return true;
                    }

                    if (e.type === 'drop') {
                        e.stopPropagation();
                        placeholders.filter(':visible').after(dragging);
                        dragging.trigger('dragend');
                        return false;
                    }
                    e.preventDefault();
                    e.originalEvent.dataTransfer.dropEffect = 'move';

                    if (items.is(this)) {
                        if (options.forcePlaceholderSize) {
                            placeholder.height(dragging.outerHeight());
                        }
                        dragging.hide();
                        $(this)[placeholder.index() < $(this).index() ? 'after' : 'before'](placeholder);
                        placeholders.not(placeholder).detach();
                    } else if (!placeholders.is(this) && !$(this).children(options.items).length) {
                        placeholders.detach();
                        $(this).append(placeholder);
                    }

                    placeholders.css({ 'width' : $(this).find('li').width(), 'height' : $(this).find('li').height() });

                    return false;
                });
        });
    };
})(jQuery);