(function($) {

var globalSettings = {};

$.fn.customSelect = function(settings) {
    settings = settings || {};
    var default_settings = {
        menuClass: 'select'
    };

    settings = globalSettings = $.extend(settings, default_settings);
    console.log("jquery.select: Custom select initialized.");
    $(this).each( function() {

        if( !$(this).is("select") ) throw "Expected select tag, got " + $(this).prop("tagName");

        globalSettings.selectInput = $(this);

        var options = [],
            optionsText = [],
            optionsVal = [];
        var default_options = {
            menuClass: 'select'
        };

        $(this).find('option').each( function() {
            var el = $(this),
                val = el.attr('value'),
                label = el.html();

            options.push({
                value: val,
                label: label,
                selected: el.attr('selected') || false
            });
        });

        var customMenu = $("<ul></ul>");
        customMenu  .addClass(settings.menuClass)
                    .addClass('hidden-options');

        // If there isn't selected option in the array
        // select the first one
        if( !haveSelected(options) ) {
            options[0].selected = true;
        }

        iterate( options, function(element, index) {
            var menuEl = $("<li></li>"),
                link = $("<a></a>");
            menuEl.html(element.label);
            if( element.selected ) menuEl.addClass('selected');
            link.attr('href', '#'+element.value);
            link.text(element.label);
            menuEl.html(link);
            customMenu.append(menuEl);
        });

        // Insert the custom select menu and hide the default one
        $(this) .after(customMenu)
                .hide();

        setEvents( customMenu );
    });
};

var iterate = function( array, exec ) {
    for( var i = 0; i < array.length; i++ ) {
        exec(array[i], i);
    }
};

var haveSelected = function( options ) {
    var haveSelected = false;
    iterate( options, function(element, index) {
        haveSelected = haveSelected || element.selected;
    });
    return haveSelected;
};

function openList(selectList) {
    selectList.find('li:not(.selected)').show();
    selectList.removeClass("hidden-options");
}

function hideList( selectList, clickedElement ) {
    selectElement(selectList, clickedElement);

    selectList.find('li:not(.selected)').hide();
    selectList.addClass("hidden-options");
}

function selectElement( selectList, el ) {
    var firstElement = selectList.find('li:first'),
        clickedElement = el || firstElement,
        selectedValue = null;

    selectedElement = selectList.find('li.selected');
    selectedElement.removeClass('selected');

    clickedElement.addClass('selected');
    clickedElement.insertBefore(firstElement);
    var hrefAttr = clickedElement.find('a').attr('href');
    selectedValue = hrefAttr.slice(1, hrefAttr.length);
    globalSettings.selectInput.val(selectedValue);
}

function setEvents( selectList ) {

    selectList.find('li').on('click', function(e) {
        e.preventDefault();
        if( selectList.hasClass("hidden-options") && $(this).hasClass("selected") ) {
            openList(selectList);
        } else {
            hideList(selectList, $(this));
        }
    });

    $(document).on('click', function(e) {
        var target = $(e.target);
        if( !target.hasParent("."+globalSettings.menuClass) ) {
            if( ! selectList.hasClass("hidden-options") ) {
                hideList(selectList);
            }
        }
    });
}

$.fn.hasParent = function(elems) {
    elems = $(elems);
    var has = false;
    $(this).parents().andSelf().each( function() {
        if($.inArray(this, elems) != -1) {
            has = true;
            return false;
        }
    });
    return has;
};

})(jQuery);