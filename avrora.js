/**
 * Avrora OS autoloader file
 */

define([
	'underscore',
	'backbone',
	'config/all',
	'modules/all'
],

function(_, Backbone, config, modules) {
	var avrora = {
		// Add the config files
		config: config,

		// Add the modules
		modules: modules,

		// Add the services
		services: modules.services,

		// Add the applications
		applications: modules.applications,

		// Avrora OS' namespace
		namespace: { models: {}, collections: {}, views: {} },

		// Account's data placeholder
		account: {},

		// Offline mode variable
		offline: false,

		/**
		 * Prints a log in the console
		 *
		 * @param  mixed  statement
		 * @return void
		 */
		log: function(statement) {
			if (this.config.get('application.debug')) {
				console.log(statement);
			}
		}
	};

	_.extend(avrora, Backbone.Events);
	_.extend(avrora.applications, Backbone.Events);
	_.extend(avrora.services, Backbone.Events);
	Backbone.Relational.store.addModelScope(avrora.namespace);

	return avrora;
});