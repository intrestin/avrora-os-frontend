/**
 * Avrora OS router file
 */

define([
    'jquery',
    'underscore',
    'backbone',
    'plugins/jquery.preload',
    'plugins/fastclick',
    'plugins/html2canvas',

    'avrora'
],

function($, _, Backbone, preload, FastClick, html2canvas, avrora) {
    var Router = Backbone.Router.extend({
        routes: {
            '' : 'home',
            'welcome' : 'welcome',
            'login' : 'login',
            'logout' : 'logout',
            'desktop' : 'desktop'
        },
        users: { length: 0 },

        home: function() {
            var that = this;

            $('#loading-text').fadeIn('fast', function() {
                avrora.modules.load(
                    avrora.config.get('application.installedModules'),
                    avrora.config.get('application.builtinApplications'),
                    function() {
                        avrora.account = avrora.modules.get('auth').model('account', true);

                        avrora.account.fetch({
                            complete: function() {
                                that.users = avrora.account.get('users');
                                that.navigate((that.users.length) ? '#/login' : '#/welcome', {trigger: true});
                            }
                        });
                    }
                );
            });
        },

        welcome: function() {
            if (this.users.length) return this.navigate('/');

            avrora.modules.get('auth').getView('welcome', {
                collection: this.users
            }).render();
        },

        login: function() {
            if (!this.users.length) window.location.reload();
            if (avrora.services.session.isAuthenticated()) return this.navigate('#/desktop');

            avrora.modules.get('auth').getView('login', {
                collection: this.users
            }).render();
        },

        logout: function() {
            if (!avrora.services.session.isAuthenticated()) return this.navigate('/');

            avrora.account.currentUser.signOut();
            _.defer(function() {
                window.location.reload();
            });
        },

        desktop: function() {
            if (!avrora.services.session.isAuthenticated()) return this.navigate('/');

            $('#welcome-text').fadeIn('slow');

            avrora.modules.get('desktop').getView('main', {
                model: avrora.account.currentUser
            }).render();
        }
    });

    var initialize = function(){
        avrora.router = new Router();
        window.location.hash = '/';

        Backbone.history.start();

        // Activate fastclick for mobile devices
        new FastClick(document.body);
    };

    return {
        initialize: initialize
    };
});
