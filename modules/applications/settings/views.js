define([
    'underscore',
    'backbone',
    'handlebars',
    'plugins/jquery.transit',
    'plugins/jquery.preload',

    'avrora',

    // Templates
    'text!applications/settings/templates/account.html',
    'text!applications/settings/templates/customization.html',
    'text!applications/settings/templates/social.html',
    'text!applications/settings/templates/privacy.html',
    'text!applications/settings/templates/system.html',
    'text!applications/settings/templates/settings.html',

    // Partials
    'text!applications/settings/templates/partials/wallpaper-container.html'
],

function(_, Backbone, Handlebars, transit, preload, avrora, asTemplate, csTemplate, ssTemplate, psTemplate, sysTemplate, settingsTemplate, wallpaperPartial) {

    var AccountSettingsView = Backbone.View.extend({

        events: {
            'click #account-settings-change-password' : 'changePasswordStart',
            'click #change-password-step1'            : 'changePasswordStep1',
            'click #change-password-step2'            : 'changePasswordStep2'
        },

        initialize: function() {
            this.collection = avrora.account.currentUser;
            new avrora.services.inputs(this);

            // Boxes
            this.passwordBox();
        },

        render: function() {
            var context = {
                    account: this.collection.toJSON(),
                    passwordBox: this._passwordBox,
                    inputs: this.Inputs
                },
                template = Handlebars.compile(asTemplate)(context);
            this.$el.html(template);

            if (this.Inputs.hasErrors()) {
                this.$el.find('input.error').first().focus();
            } else {
                this.$el.find('.content-box').css({opacity : 0, x : '300px'})
                    .transition({x : '0', opacity : 1}, 500, 'snap');
            }

            return this;
        },

        /* View events */

        changePasswordStart: function(e) {
            this.passwordBox(true).render();

            return false;
        },

        changePasswordStep1: function(e) {
            var passwordValue = this.$el.find('input[name=password_old]').val(),
                currentPassword = this.collection.get('password'),
                rules = { 'password_old' : 'required' };

            if (this.Inputs.validate(rules)) {
                if (currentPassword !== this.getPasswordHash(passwordValue)) {
                    this.Inputs.addValidationError('password_old', 'This is not your password.');
                } else {
                    this.passwordBox(true, false, true);
                }
            }

            this.render();

            return false;
        },

        changePasswordStep2: function(e) {
            var passwordValue = this.$el.find('input[name=password]').val(),
                currentPassword = this.collection.get('password'),
                rules = { 'password' : 'required|confirmed', 'password_confirmed' : 'required' };

            if (this.Inputs.validate(rules)) {
                var hashedPassword = this.getPasswordHash(passwordValue);
                if (hashedPassword !== currentPassword) {
                    this.collection.set('password', hashedPassword).save();
                }

                this.passwordBox();
            }

            this.render();

            return false;
        },

        /* View methods */

        passwordBox: function(active, step1, step2) {
            if (_.isUndefined(active)) active = false;

            var _step1 = !!(this.collection.get('password'));
            if (!_.isUndefined(step1)) _step1 = step1;

            var _step2 = !_step1;
            if (!_.isUndefined(step2)) _step2 = step2;

            if (_step1 === _step2) return;

            this._passwordBox = {
                active: active,
                step1: _step1,
                step2: _step2
            };

            return this;
        },

        getPasswordHash: function(password) {
            return avrora.services.hashers.sha1(password);
        }
    });

    Handlebars.registerHelper('wallpapers', function(source) {
        var out = '',
            that = this,
            wallpapers = [];

        if (source === 'my') {
            if (this.userWallpapers.length) wallpapers = this.userWallpapers;
            else out = '<p>You have no wallpapers.</p>';
        } else if (source === 'internet') {
            if (this.internetWallpapers.length) wallpapers = this.internetWallpapers;
            else out = '<p>Loading...</p>';
        } else if (source === 'solid') {
            wallpapers = this.solidColors;
        }

        if (wallpapers.length) {
            _.each(wallpapers, function(wallpaper) {
                var full = (_.isString(wallpaper)) ? wallpaper : wallpaper.full,
                    thumb = (_.isString(wallpaper)) ? wallpaper : wallpaper.thumb;

                var context = {
                        full: full,
                        thumb: thumb,
                        current: (that.account.wallpaper === full),
                        color: (full[0] === '#')
                    };

                out += Handlebars.compile(wallpaperPartial)(context);
            });
        }

        return new Handlebars.SafeString(out);
    });

    var CustomizationSettingsView = Backbone.View.extend({

        events: {
            'click .wallpaper-source li'           : 'changeTab',
            'click .wallpaper-tabs .wallpaper'     : 'chooseWallpaper',
            'click #wallpapers-internet-load-more' : 'nextWallpaperPage'
        },

        initialize: function() {
            var that = this;
            this.collection = avrora.account.currentUser;

            this.userWallpapers = [avrora.config.get('assets.defaultWallpaper')];

            this.internetWallpapers = [];
            this.internetWallpapersLoaded = 0;
            this.internetWallpapersPage = 1;
            this.visitedPages = [];

            this.solidColors = ['#69D2E7', '#A7DBD8', '#E0E4CC', '#F38630', '#FA6900',
                                '#ECD078', '#D95B43', '#C02942', '#542437', '#53777A',
                                '#556270', '#EDEDED', '#C7F464', '#FF6B6B', '#303030',
                                '#490A3D', '#BD1550', '#E97F02', '#F8CA00', '#8A9B0F'];

            this.currentTab = '#wallpapers-my';
            this.currentWallpaper = this.collection.get('wallpaper');

            this.collection
                .on('change:wallpaper', function(model) {
                    var wallpaper = model.get('wallpaper'),
                        wallpaperContainer = that.$el.find('.current-wallpaper');

                    if (wallpaper[0] === '#') {
                        wallpaperContainer.css('background-color', wallpaper);
                    } else {
                        wallpaperContainer.css('background-image', 'url(' + wallpaper + ')');
                    }
                });
        },

        render: function() {
            var context = {
                    account: this.collection.toJSON(),
                    userWallpapers: this.userWallpapers,
                    internetWallpapers: this.internetWallpapers,
                    solidColors: this.solidColors,
                    currentWallpaper: this.currentWallpaper,
                    colorBackground: (this.currentWallpaper[0] === '#')
                },
                template = Handlebars.compile(csTemplate)(context);
            this.$el.html(template);
            this.$el.find('a[href="' + this.currentTab + '"]').click();

            return this;
        },

        /* View events */

        changeTab: function(e) {
            var $target = $(e.target);
            if (!$target.attr('href')) $target = $target.find('a');

            var tab = $target.attr('href');

            if ($target.hasClass('active')) return;

            this.$el.find('.wallpaper-source .active').removeClass('active');
            $target.parent().addClass('active');

            this.$el.find('.wallpaper-tabs .active').removeClass('active');
            $(tab).addClass('active');

            this.currentTab = tab;

            this.$el.find('#wallpapers-internet-load-more').hide();
            if (tab === '#wallpapers-internet') this.loadInternetWallpapers();

            return false;
        },

        chooseWallpaper: function(e) {
            var $target = $(e.target),
                url = $target.data('url');

            if (!url) return;

            this.$el.find('.wallpaper.current').removeClass('current').html('');
            $target.addClass('current').html('<div class="current">Current</div>');
            this.currentWallpaper = url;

            this.collection.set('wallpaper', url);
            this.collection.save();

            return false;
        },

        nextWallpaperPage: function(e) {
            this.internetWallpapersLoaded = 0;
            this.loadInternetWallpapers();

            return false;
        },

        /* View methods */

        loadInternetWallpapers: function() {
            if (this.internetWallpapersLoaded === 1) this.$el.find('#wallpapers-internet-load-more').show();
            if (this.internetWallpapersLoaded !== 0) return;

            var $tab = this.$el.find('#wallpapers-internet'),
                that = this,
                context = {
                    placeholder: true
                },
                loaderPlaceholder = Handlebars.compile(wallpaperPartial)(context),
                wallpaperCount = 0;

            this.$el.find('#wallpapers-internet-load-more').hide();

            this.internetWallpapersLoaded = 2;
            $.getJSON(avrora.config.get('assets.internetWallpapersApi') + '?page=' + this.internetWallpapersPage, function(data) {
                if (!$tab.find('.wallpaper').length) $tab.html('');
                $tab.append(loaderPlaceholder);

                that.visitedPages.push(that.internetWallpapersPage);

                // Generate next random page
                var random = _.random(2, data.pagination.pages);
                while (_.indexOf(that.visitedPages, random) !== -1) {
                    random = _.random(2, data.pagination.pages);
                }
                that.internetWallpapersPage = random;

                _.each(data.response, function(wallpaper) {
                    $.preload(
                        [wallpaper.image.url, wallpaper.image.thumb.url],
                        {
                            onFinish: function() {
                                $tab.find('.placeholder').remove();

                                that.internetWallpapers.push({
                                    thumb: wallpaper.image.thumb.url,
                                    full: wallpaper.image.url
                                });

                                var context = {
                                        full: wallpaper.image.url,
                                        thumb: wallpaper.image.thumb.url,
                                        current: (wallpaper.image.url === that.currentWallpaper)
                                    },
                                    wallpaperHtml = Handlebars.compile(wallpaperPartial)(context);

                                $tab.append(wallpaperHtml);
                                $tab.append(loaderPlaceholder);

                                wallpaperCount++;
                                if (data.response.length === wallpaperCount) {
                                    $tab.find('.placeholder').remove();
                                    that.$el.find('#wallpapers-internet-load-more').show();
                                    that.internetWallpapersLoaded = 1;
                                }
                            }
                        }
                    );
                });
            });
        }
    });

    var SocialSettingsView = Backbone.View.extend({

        render: function() {
            var template = Handlebars.compile(ssTemplate);
            this.$el.html(template);

            return this;
        }
    });

    var PrivacySettingsView = Backbone.View.extend({

        render: function() {
            var template = Handlebars.compile(psTemplate);
            this.$el.html(template);

            return this;
        }
    });

    var SystemSettingsView = Backbone.View.extend({

        render: function() {
            var template = Handlebars.compile(sysTemplate);
            this.$el.html(template);

            return this;
        }
    });

    var SettingsView = Backbone.View.extend({

        events: {
            'click #settings-app-content-menu ul li' : 'clickSetting'
        },

        initialize: function() {
            this.settings = {
                account : {
                    title : 'Account',
                    trigger : 'account',
                    view : AccountSettingsView,
                    active : true,
                    rendered : false
                },
                customization : {
                    title : 'Customization',
                    trigger : 'customization',
                    view : CustomizationSettingsView,
                    active : false,
                    rendered : false
                },
                social : {
                    title : 'Social',
                    trigger : 'social',
                    view : SocialSettingsView,
                    active : false,
                    rendered : false
                },
                privacy : {
                    title : 'Privacy',
                    trigger : 'privacy',
                    view : PrivacySettingsView,
                    active : false,
                    rendered : false
                },
                system : {
                    title : 'System',
                    trigger : 'system',
                    view : SystemSettingsView,
                    active : false,
                    rendered : false
                }
            };
        },

        render: function(noTrigger) {
            var currentSetting = this.getCurrentActive();

            var template = Handlebars.compile(settingsTemplate)({
                currentSetting: currentSetting,
                settings: this.settings
            });
            this.$el.html(template);

            this.$el.find('.right').css({opacity : 0, x : '400px'})
                    .transition({x : '0', opacity : 1}, 500, 'snap');

            if (_.isUndefined(noTrigger)) {
                this.triggerSetting(currentSetting.trigger, false);
            }

            return this;
        },

        /* View events */

        clickSetting: function(e) {
            var $target = $(e.target);
            if (!$target.data('trigger')) $target = $target.parents('li');
            if (!$target.data('trigger')) return;

            // Trigger wanted setting
            this.triggerSetting($target.data('trigger'), true);

            return false;
        },

        /* View methods */

        triggerSetting: function(settingName, triggered) {
            if (_.isUndefined(settingName)) return;

            var item = this.settings[settingName],
                View = item.view;

            if (item.active && item.rendered && triggered) return;
            if (!item.rendered) item.rendered = new View();

            this.makeAllInactive(item.trigger);
            if (triggered) this.render(true);

            item.rendered.setElement(this.$el.find('#settings-app-content-box'));
            item.rendered.render();
        },

        getCurrentActive: function() {
            return _.filter(this.settings, function(setting) {
                return setting.active;
            })[0];
        },

        makeAllInactive: function(exception) {
            _.each(this.settings, function(setting) {
                setting.active = (exception === setting.trigger);
            });
        }
    });

    return {
        main: SettingsView
    };
});