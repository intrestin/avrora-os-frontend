define([
    'underscore',

    'avrora'
],

function(_, avrora) {

    return {
        'name' : 'Settings',
        'uid' : 'settings', // app's unique id
        'icon' : 'wrench',
        'speech' : 'настройки',
        'placement' : 'taskbar' // toolbar, taskbar only (atm)
    };
});