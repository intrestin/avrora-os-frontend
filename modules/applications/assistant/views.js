define([
    'underscore',
    'backbone',
    'handlebars',

    'avrora',

    // Templates
    'text!applications/assistant/templates/assistant.html',
    'text!applications/assistant/templates/results.html'
],

function(_, Backbone, Handlebars, avrora, assistantTemplate, resultsTemplate) {

    Handlebars.registerHelper('ifTypeIs', function(type, options) {
        if (this.type === type) return options.fn(this);
        else return options.inverse(this);
    });

    var AssistantView = Backbone.View.extend({

        events: {
            'click #results-container li' : 'chooseResult'
        },

        initialize: function() {
            var that = this;

            this.applications = avrora.modules.get('desktop.collections.applications');

            _.defer(function() {
                that.applications.fetch();
            });
        },

        render: function() {
            var template = Handlebars.compile(assistantTemplate)();
            this.$el.html(template);

            return this;
        },

        afterRender: function() {
            var $avroraInput = this.$el.find('input'),
                that = this;

            $avroraInput
                .focus()
                .off('keyup').off('keydown').off('input')
                .on('keydown input', _.debounce(function(e) {
                    if (e.which === 38 || e.which === 40) return;
                    that.parseInput($avroraInput.val());
                }, 200))
                .on('keyup', function(e) {
                    if (e.which === 38 || e.which === 40) {
                        if (e.which === 38) {
                            that.chooseResult(0);
                        } else {
                            that.chooseResult(1);
                        }
                    }
                    if (e.which === 13) {
                        that.chooseResult();
                    }
                });
        },

        parseInput: function(input, chat) {
            input = input.toUpperCase().trim().replace(/[!@#$%^&*()_+-=]/, '').replace(/\s\s+/gi, ' ');
            chat = chat || false;

            if (!input.length) {
                this.$el.find('#results-container').html('').hide();
                return false;
            }

            this.respond(input, chat);
        },

        respond: function(input, chat) {
            var results = this.getResults(input),
                context = {
                    results: results,
                    resultsLength: results.length
                };

            if (!results.length) return;

            if (!chat) {
                var resultsContent = Handlebars.compile(resultsTemplate)(context);

                var $results = this.$el.find('#results-container');
                $results.html(resultsContent).show();
                $results.find('.active').removeClass('active');
                $results.find('li').first().addClass('active');
            } else {

            }
        },

        getResults: function(input) {
            var results = [],
                result = function(data) {
                    return {
                        title : data.title || '',
                        icon : data.icon || '',
                        id : data.id || '',
                        type : data.type || 0
                    };
                };

            // Get results for applications
            var applications = this.applications.getByName(input, true);
            _.each(applications, function(app) {
                var data = {
                    title: app.get('name'),
                    icon: app.get('icon') || app.get('icon_url'),
                    id: app.get('url'),
                    type: 0
                };
                results.push(result(data));
            });

            // Get results for builtin applications
            var builtinApplications = avrora.modules.get('desktop.collections.taskbaritems').getByTitle(input, true);
            _.each(builtinApplications, function(app) {
                if (app.get('type') !== 'application') return;
                if (app.get('data') === 'assistant') return;

                var data = {
                    title: app.get('title'),
                    icon: app.get('icon'),
                    id: app.get('data'),
                    type: 1
                };
                results.push(result(data));
            });

            return results;
        },

        chooseResult: function(next) {
            var $results = this.$el.find('#results-container'),
                resultsLength = $results.find('li').length;

            if (resultsLength && $results.find('li.active')) {
                if (_.isUndefined(next) || !_.isBoolean(next)) {
                    var type = $results.find('li.active').data('type'),
                        id = $results.find('li.active').data('id');

                    if (type === 0 || type === 1) {
                        if (type === 0) {
                            var app = this.applications.getApplicationByUrl(id);
                            if (!app.length) return;
                            app = app[0];

                            if (!avrora.account.currentUser.hasInstalledApp(app)) {
                                id = 'market';
                                type = 1;
                            }
                        }

                        avrora.account.currentUser.openApplication(id, type);
                    }

                    return;
                }

                var currentActiveIndex = $results.find('li.active').index();
                $results.find('li.active').removeClass('active');

                if (next) {
                    currentActiveIndex = (currentActiveIndex + 1 >= resultsLength) ? 0 : currentActiveIndex + 1;
                } else {
                    currentActiveIndex = (currentActiveIndex - 1 < 0) ? resultsLength - 1 : currentActiveIndex - 1;
                }

                $($results.find('li').get(currentActiveIndex)).addClass('active');

                var scrollTop = $results.scrollTop() + $results.find('li.active').position().top - $results.height()/2 + $results.find('li.active').height()/2;
                $results.scrollTop(scrollTop);
            }
        }
    });

    return {
        main: AssistantView
    };
});