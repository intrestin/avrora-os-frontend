define([
    'underscore',

    'avrora'
],

function(_, avrora) {

    return {
        'name' : 'Avrora',
        'uid' : 'assistant', // app's unique id
        'icon' : 'avrora',
        'speech' : 'avrora',
        'placement' : 'taskbar', // toolbar, taskbar only (atm)
        'modal' : {
            'width' : 400,
            'height' : 'auto',
            'maxheight' : 300
        }
    };
});