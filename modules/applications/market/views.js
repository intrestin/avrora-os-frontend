define([
    'underscore',
    'backbone',
    'handlebars',
    'plugins/jquery.preload',
    'plugins/jquery.transit',

    'avrora',

    // Templates
    'text!applications/market/templates/market.html'
],

function(_, Backbone, Handlebars, preload, transit, avrora, marketTemplate) {

    Handlebars.registerHelper('installButton', function() {
        var installed = false;
        if (avrora.account.currentUser.hasInstalledApp(this)) installed = true;

        return '<button class="button small ' + ((installed) ? 'error' : '') +
                '" data-app="' + this.resource_uri + '" id="market-app-' +
                ((installed) ? 'un' : '') + 'install-button">' +
                    ((installed) ? 'Remove' : 'Install') +
                '</button>';
    });

    var MarketView = Backbone.View.extend({

        events: {
            'click #market-app-install-button'   : 'installApplication',
            'click #market-app-uninstall-button' : 'uninstallApplication'
        },

        initialize: function() {
            var that = this;

            this.marketAppsColors = {};
            this.collection = avrora.modules.get('desktop.collections.applications');
            this.collection
                .on('sync', function() {
                    that.render();
                });

            avrora.account.currentUser.get('applications')
                .on('add', function(app) {
                    var $installedApp = that.getMarketAppContainer(app);
                    if ($installedApp.length) that.changeMarketButton($installedApp, true);
                })
                .on('remove', function(app) {
                    var $installedApp = that.getMarketAppContainer(app);
                    if ($installedApp.length) that.changeMarketButton($installedApp, false);
                });

            _.defer(function() {
                that.collection.fetch();
            });
        },

        render: function() {
            var template = Handlebars.compile(marketTemplate)({
                applications: this.collection.toJSON()
            });
            this.$el.html(template);

            this.prepareApplicationBoxes();

            return this;
        },

        /* View events */

        installApplication: function(e) {
            var $target = $(e.target),
                application = $target.data('application'),
                currentUser = avrora.account.currentUser;

            if (!currentUser.hasInstalledApp(application) && !$target.data('clicked')) {
                currentUser.installApplication(application);
                $target.data('clicked', true);

                _.delay(function() {
                    $target.data('clicked', false);
                }, 1000);
            }

            return false;
        },

        uninstallApplication: function(e) {
            var $target = $(e.target),
                application = $target.data('application'),
                currentUser = avrora.account.currentUser;

            if (currentUser.hasInstalledApp(application) && !$target.data('clicked')) {
                currentUser.uninstallApplication(application);
                $target.data('clicked', true);

                _.delay(function() {
                    $target.data('clicked', false);
                }, 1000);
            }

            return false;
        },

        /* View methods */

        prepareApplicationBoxes: function() {
            var that = this,
                $marketApps = this.$el.find('.market-app'),
                getAvarageColor = _.memoize(this.getAvarageColor);

            if (!$marketApps.length) return;

            var appIcons = [];
            _.each($marketApps, function(app) {
                var $appButton = $(app).find('button'),
                    resourceUri = $appButton.attr('data-app');

                appIcons.push($(app).find('img').attr('src'));

                var application = that.collection.getApplicationByUri(resourceUri);
                if (!application.length) return;

                $appButton.data('application', application[0]);
            });

            $.preload(appIcons, {
                onFinish: function() {
                    _.each($marketApps, function(app) {
                        var $appButton = $(app).find('button'),
                            resourceUri = $appButton.attr('data-app');

                        if (!(resourceUri in that.marketAppsColors)) {
                            _.defer(function() {
                                that.marketAppsColors[resourceUri] = getAvarageColor(resourceUri);
                            });
                        }
                    });

                    if (_.keys(that.marketAppsColors).length) {
                        that.setAvarageColors();
                    } else {
                        _.defer(function() {
                            that.setAvarageColors();
                        });
                    }
                }
            });
        },

        getAvarageColor: function(resourceUri) {
            var $appButton = $('button[data-app="' + resourceUri + '"]'),
                $image = $appButton.parents('.market-app').find('img');

            if ($image.hasClass('external')) return;
            if (!$image.hasClass('internal')) return;

            var canvas,
                context,
                image,
                imageData;

            try {
                canvas = document.createElement('canvas');
                context = canvas.getContext('2d');
                image = document.createElement('img');

                image.src = $image.attr('src');

                $(canvas).attr('width', image.width);
                $(canvas).attr('height', image.height);

                context.drawImage(image, 0, 0, image.width, image.height);
                imageData = context.getImageData(0, 0,  image.width, image.height);
            } catch(e) {
                return false;
            }

            var imageDataArray = imageData.data,
                imageDataArrayLen = imageDataArray.length,
                colors=[0,0,0];

            for (var i = 0; i < imageDataArrayLen; i += 4) {
                colors[0] += imageDataArray[i];
                colors[1] += imageDataArray[i+1];
                colors[2] += imageDataArray[i+2];
            }

            // Red
            colors[0] /= (imageDataArrayLen/3);
            colors[0] = Math.round(colors[0]);

            // Green
            colors[1] /= (imageDataArrayLen/3);
            colors[1] = Math.round(colors[1]);

            // Blue
            colors[2] /= (imageDataArrayLen/3);
            colors[2] = Math.round(colors[2]);

            return colors;
        },

        setAvarageColors: function() {
            var that = this;

            _.each(_.keys(this.marketAppsColors), function(resourceUri) {
                var colors = that.marketAppsColors[resourceUri];
                if (!colors) return;

                var $appButton = $('button[data-app="' + resourceUri + '"]'),
                    $marketApp = $appButton.parents('.market-app');
                $appButton.removeAttr('data-app');

                $marketApp.transition({'background-color' : 'rgb(' + colors[0] + ',' + colors[1] + ',' + colors[2]+')'}, 300, 'snap');
            });
        },

        changeMarketButton: function($marketApp, install) {
            var $button = $marketApp.find('button'),
                id = $button.attr('id');

            if (!id) return;
            if (install) {
                $button.addClass('error');
                $button.html('Remove').attr('id', id.replace('install', 'uninstall'));
            } else {
                $button.removeClass('error');
                $button.html('Install').attr('id', $button.attr('id').replace('uninstall', 'install'));
            }
        },

        getMarketAppContainer: function(app) {
            return this.$el.find('.market-app').filter(function() {
                if (!$(this).find('button').data('application')) return false;
                return $(this).find('button').data('application').url() === app.url();
            });
        }
    });

    return {
        main: MarketView
    };
});