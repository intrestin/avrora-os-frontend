define([
    'underscore',

    'avrora'
],

function(_, avrora) {

    return {
        'name' : 'Avrora Market',
        'uid' : 'market', // app's unique id
        'icon' : 'cart',
        'speech' : 'маркет',
        'placement' : 'taskbar' // toolbar, taskbar only (atm)
    };
});