define([
    'underscore',

    'avrora'
],

function(_, avrora) {

    var recognizing = false;

    settings = {
        websocketsServer: "ws://localhost:8080/websocket"
    };

    var ws = new WebSocket(settings.websocketsServer);

    var regexpsBG = {
        login: '^(вход|влез|работен плот)',
        saySomething: 'кажи нещо',
        changePassword: '(смяна на паролата|промяна на паролата)',
        open: '^(отвори|стартирай)',
        search: '^(търси|намери|потърси)',
        close: '^(затвори|спри)',
        maximize: '^(разгърни|на цял екран)',
        hideAll: 'скрий всички',
        playMusic: 'пусни малко музика',
        sleep: 'заспивай',
        logout: 'изход'
    };

    var regexpsEN = {
        login: '^(login|desktop|sign\\sin)',
        open: '^(open|start)',
        search: '^(search|find|lookup)',
        close: '^(close|stop)',
        maximize: '^(maximize|fullscreen)',
        hideAll: 'hide all',
        playMusic: 'play some music',
        sleep: 'sleep',
        logout: 'logout'
    };

    var bgDict = {
        firstApp: 'първото приложение',
        thisApp: '(текущото|това)\\s(приложение|приложения)',
        safari: 'сафари',
        twitter: 'туитър',
        skype: 'скайп',
        vlc: 'vlc',
        minecraft: 'minecraft'
    };

    var enDict = {
        firstApp: 'first application',
        thisApp: '(current|this)\\s(application|app)',
        safari: 'Safari',
        twitter: 'Twitter',
        skype: 'Skype',
        vlc: 'vlc',
        minecraft: 'minecraft'
    };

    var tryMatch = function(command, transcript) {
        var regex = new RegExp(command, 'i');
        if (regex.test(transcript)) return true;

        return false;
    };

    var prepCommand = function(command, startFrom) {
        var concatStrs = function(memo, str) {
            return memo + ' ' + str;
        };
        startFrom = startFrom || 1;
        command = command.split(' ').splice(startFrom, command.length-1);
        command = $.trim(_.reduce(command, concatStrs, ''));
        return command;
    };

    var actions = {
        login: function() {
            $('a.avatar').click();
            if ($('#login-box input[type=password]').css('display') === 'block') {
                $('#login-box button').click();
            }
        },

        saySomething: function() {
            avrora.log('Hey, how are you?');
        },

        logout: function() {
            $('li[data-itemlink="logout"]').click();
        },

        playMusic: function() {
            ws.send("osascript -e 'tell application \"iTunes\" to play the playlist named \"Welcome To The Jungle\"'");
        },

        sleep: function() {
            ws.send("osascript -e 'tell application \"Finder\" to sleep'");
        },

        changePassword: function() {
            window.location = '/account/change/password/';
        },

        openApp: function(element) {
            element.click();
        },

        maximize: function(command) {
            command = prepCommand(command, 1);

            // Remove boilerplate code
            var appUrl = avrora.applications.getAllItems().filter(
                function(app) {
                    return app.config.speech === command;
                }
            );
            if (!appUrl.length) return;

            var application = avrora.modules
                                    .get('desktop.collections.activeapps')
                                    .getByUrl('#/' + appUrl[0].config.url)[0];

            avrora.modules
                .get('desktop.views.applicationmanager')
                .toggleMaximize(application);
        },

        hideAll: function(command) {
            avrora.log(command);
            avrora.modules.get('desktop.views.applicationmanager').showDesktop();
        },

        handleCloseCommand: function(command) {
            avrora.log(command);
            var closeOptions = {
                thisApp: function() {
                    avrora.modules
                        .get('desktop.views.applicationmanager')
                        .removeApplication();
                },
                safari: function() {
                    ws.send("osascript -e 'tell application \"Safari\" to quit'");
                },
                skype: function() {
                    ws.send("osascript -e 'tell application \"Skype\" to quit'");
                },
                twitter: function() {
                    ws.send("osascript -e 'tell application \"Twitter\" to quit'");
                },
                vlc: function() {
                    ws.send("osascript -e 'tell application \"VLC\" to quit'");
                }
            };

            // TODO: Remove boilerplate code
            _.each(_.keys(closeOptions), function(curr) {
                if (_.has(enDict, curr)) {//if (_.has(bgDict, curr)) {
                    var regex = new RegExp(enDict[curr]); // var regex = new RegExp(bgDict[curr]);
                    if (regex.test(command)) {
                        closeOptions[curr]();
                    }
                }
            });

            command = prepCommand(command, 1);
            var appUrl = avrora.applications.getAllItems().filter(
                function(app) {
                    return app.config.speech === command;
                }
            );
            if (!appUrl.length) return;

            var application = avrora.modules.get('desktop.collections.activeapps').filter(
                function(app) {
                    return app.get('url') === '#/' + appUrl[0].config.url;
                }
            );

            if (application.length) application[0].destroy();
        },

        search: function(command) {
            avrora.log('Searching for something.');
            command = prepCommand(command, 1);
            $('[data-url=http:\/\/duckduckgo.com]').click();
        },

        handleOpenCommand: function(command) {
            var openOptions = {
                    firstApp: function() {
                        $('ul.apps li:first').click();
                    },
                    safari: function() {
                        ws.send('open Safari.app');
                    },
                    skype: function() {
                        ws.send('open Skype.app');
                    },
                    twitter: function() {
                        ws.send('open Twitter.app');
                    },
                    vlc: function() {
                        ws.send('open VLC.app');
                    },
                    minecraft: function() {
                        ws.send('open Minecraft.app');
                    }
                },
                commandResolved = false;

            command = prepCommand(command);

            _.each(_.keys(openOptions), function(curr) {
                if (_.has(bgDict, curr)) {
                    if (bgDict[curr] === command) {
                        openOptions[curr]();
                        commandResolved = true;
                    }
                }
            });

            $('[data-application]').each( function() {
                var element = $(this).find('i'),
                    dataSpeech = element.attr('data-speech');

                if (!_.isUndefined(dataSpeech) && dataSpeech !== false) {
                    // var lang = element.attr('data-speech-lang');
                    if (tryMatch('^'+dataSpeech+'$', command)) {
                        avrora.log('Openning custom app.');
                        actions.openApp(element);
                        commandResolved = true;
                        avrora.log(element);
                    }
                }
            });
        }
    };

    var globalCommands = {
        login: function() { actions.login(); },
        saySomething: function() { actions.saySomething(); },
        changePassword: function() { actions.changePasswordPage(); },
        open: function(transcript) { actions.handleOpenCommand(transcript); },
        search: function(transcript) { actions.search(transcript); },
        maximize: function(transcript) { actions.maximize(transcript); },
        hideAll: function(transcript) { actions.hideAll(transcript); },
        playMusic: function(transcript) {actions.playMusic(transcript); },
        sleep: function(transcript) { actions.sleep(transcript); },
        close: function(transcript) { actions.handleCloseCommand(transcript); },
        logout: function() { actions.logout(); }
    };

    var speech = {

        init: function() {
            var that = this;

            this.toggleListener();

            if (!('webkitSpeechRecognition' in window)) {
                avrora.log('Speech is not supported!');
            } else {
                var recognition = new webkitSpeechRecognition(),
                    grammar = new webkitSpeechGrammar();

                grammar.src = '/static/speech/grammar/basic.grxml';
                recognition.grammars[0] = grammar;
                recognition.continious = false;
                recognition.interimResults = false;
                recognition.lang = 'en-EN'; // bg-BG

                recognition.onstart = function() {
                    that.speaking();
                };
                recognition.onresult = function(event) {
                    that.result(event);
                };
                recognition.onerror = function(event) {
                    that.error(event);
                };
                recognition.onend = function() {
                    that.stopped();
                };

                recognition.start();
                recognizing = true;
                avrora.log('Hey, I\'m listening to your commands.');
            }
        },

        speaking: function() {
            avrora.log('Speaking has started!');
        },

        result: function(event) {
            var that = this;

            avrora.log('Speaking: Result gathered!');
            avrora.log(event);
            var cmdTriggered = false,
                finalTranscript = '';

            finalTranscript = event.results[0][0].transcript;

            _.each(_.keys(this.normalizedCommands), function(command) {
                if (tryMatch(command, finalTranscript)) {
                    cmdTriggered = true;
                    that.normalizedCommands[command](finalTranscript);
                }
            });

            $('[data-speech]').each(function() {
                var $element = $(this),
                    command = $element.attr('data-speech');
                    // lang = $(this).attr('data-speech-lang');
                if (tryMatch('^'+command+'$', finalTranscript)) {
                    avrora.log('Openning custom app.');
                    actions.openApp($element);
                    cmdTriggered = true;
                    avrora.log($element);
                }
            });

            if (!cmdTriggered) {
                // Note: Message position is not correct?
                avrora.trigger('message', 'messages.speechNotRecognized');
            }
            recognizing = false;
        },

        error: function(event) {
            avrora.log('Speaking: Error occured!');
            avrora.log(event);
            avrora.trigger('message', 'messages.speechError');
            recognizing = false;
        },

        stopped: function() {
            avrora.log('Speaking has stopped!');
            this.toggleListener();
        },

        toggleListener: function() {
            if (!$('body').find('#listener').length) {
                var $listening = $('<div id="listener"><i class="big dark icon-microphone icon"></i></div>');
                $listening.css({
                    position: 'absolute',
                    width: '130px',
                    height: '150px',
                    'background-color': 'rgba(255, 255, 255, .7)',
                    left: '47%',
                    top: '30px',
                    display: 'none'
                });
                $('body').append($listening);
                $('#listener').fadeIn();
            } else {
                $('#listener').fadeOut(500, function() {
                    $(this).remove();
                });
            }
        },

        normalizeCommands: function(regexps) {
            var commands = {};
            var key = null;
            _.each(_.keys(globalCommands), function(curr) {
                if (_.has(regexps, curr)) {
                    key = regexps[curr];
                    commands[key] = globalCommands[curr];
                } else {
                    commands[curr] = globalCommands[curr];
                }
            });
            return commands;
        }
    };

    $(document).ready(function() {
        //speech.normalizedCommands = speech.normalizeCommands(regexpsBG);
        speech.normalizedCommands = speech.normalizeCommands(regexpsEN);
        avrora.log(speech.normalizedCommands);
    });

    return speech;

});