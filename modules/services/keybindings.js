define([
    'underscore',
    'plugins/mousetrap',

    'avrora'
],

function(_, Mousetrap, avrora) {

    var actions = {
        switchBetweenApps: function() {
            avrora.modules.get('desktop.views.applicationmanager').switchBetweenApps();
        },

        resizeLeft: function() {
            avrora.modules.get('desktop.views.applicationmanager').toggleApplicationState(2, false, true);
        },

        resizeRight: function() {
            avrora.modules.get('desktop.views.applicationmanager').toggleApplicationState(3, false, true);
        },

        resizeDown: function() {
            avrora.modules.get('desktop.views.applicationmanager').toggleApplicationState(0, false, true);
        },

        resizeUp: function() {
            avrora.modules.get('desktop.views.applicationmanager').toggleApplicationState(1, false, true);
        },

        switchFullscreen: function() {
            avrora.modules.get('desktop.views.applicationmanager').toggleFullscreen();
        },

        quitApplication: function() {
            avrora.modules.get('desktop.views.applicationmanager').removeApplication();
        },

        initSpeech: function() {
            avrora.services.speech.init();
        },

        showDesktop: function() {
            avrora.modules.get('desktop.views.applicationmanager').showDesktop();
        },

        openAssistant: function() {
            if (avrora.account.currentUser) avrora.account.currentUser.openApplication('assistant', true);
        }
    };

    var keybindings = {
        keybindings: avrora.config.get('keybindings'),
        actions: actions,

        init: function() {
            var that = this;

            _.each(_.keys(this.keybindings), function(keybinding) {
                Mousetrap.bind(keybinding, function(e) {
                    e.preventDefault();
                    that.actions[that.keybindings[keybinding]](e);
                });
            });
        }
    };

    $(function() {
        keybindings.init();
    });

    return keybindings;
});