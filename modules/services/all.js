/**
 * Avrora OS autoloader for the services
 */

define([
	'underscore',

	'modules/services/tooltip',
    'modules/services/ping',
    'modules/services/message',
    'modules/services/speech',
    'modules/services/inputs',
    'modules/services/hashers',
    'modules/services/storage',
    'modules/services/session',
    'modules/services/prototypes',
    'modules/services/keybindings'
],

function(_, tooltip, ping, message, speech, inputs, hashers, storage, session, prototypes, keybindings) {

	var services = {
		tooltip: tooltip,
        ping: ping,
        message: message,
        speech: speech,
        inputs: inputs,
        hashers: hashers,
        storage: storage,
        session: session,
        prototypes: prototypes,
        keybindings: keybindings
	};

	return services;
});