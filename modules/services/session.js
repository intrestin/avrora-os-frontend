define([
    'underscore',

    'avrora'
],

function(_, avrora) {

    var session = {
        isAuthenticated: function() {
            var savedUser = avrora.services.storage.getItem('currentUser');
            if (_.isNull(savedUser)) return false;
            if (!_.isUndefined(avrora.account.currentUser)) return true;

            var users = avrora.account.get('users'),
                resourceUri = savedUser.resource_uri,
                currentUser = users.get(resourceUri);
            avrora.account.currentUser = currentUser;

            return true;
        }
    };

    return session;
});