define([
    'underscore',

    'plugins/jquery.tooltipster'
],

function(_) {
    var getTooltipOptions = function($tooltip) {
        var position = $tooltip.data('placement') || 'bottom';
        return {
            position: position,
            trigger: $tooltip.data('trigger') || 'hover',
            delay: $tooltip.data('delay') * 1000 || 2000,
            timer: $tooltip.data('activetime') * 1000 || 6000,
            content: $tooltip.data('tooltip') || '',
            theme: '.tooltipster-' + position,
            animation: 'grow'
        };
    };

    $(function() {
        _.each($('[data-tooltip]'), function(tooltip) {
            var $tooltip = $(tooltip);
            $tooltip.tooltipster(getTooltipOptions($tooltip));
        });
        $('body').delegate('[data-tooltip]', 'mouseenter', function() {
            var $tooltip = $(this),
                tooltipOptions = getTooltipOptions($tooltip);

            $tooltip
                .removeAttr('data-tooltip')
                .tooltipster(tooltipOptions)
                .tooltipster('show');
        });
    });

    return {};
});