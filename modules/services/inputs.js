define([
    'underscore',
    'handlebars',

    'avrora'
],

function(_, Handlebars, avrora) {

    var errorMessages = avrora.config.get('errors');

    Handlebars.registerHelper('error', function(inputName) {
        var errors = this.inputs.errors;

        if (!_.has(errors, inputName)) return '';
        return new Handlebars.SafeString('<span class="error">' + errors[inputName] + '</span>');
    });

    Handlebars.registerHelper('value', function(inputName) {
        var inputs = this.inputs;

        return inputs.get(inputName, '');
    });

    Handlebars.registerHelper('errorClass', function(inputName, errorClass) {
        if (!_.has(this.inputs.errors, inputName)) return '';
        return errorClass;
    });

    var Validation = function() {
        var args = arguments[0],
            inputs = args.inputs || false,
            rules = args.rules || false;

        return {
            errors: {},

            fails: function() {
                return this.invalid();
            },

            invalid: function() {
                return !this.valid();
            },

            valid: function() {
                if (!rules) return true;

                var that = this;

                this.errors = {};

                _.each(_.keys(rules), function(inputName) {
                    that.check(inputName, rules[inputName]);
                });

                inputs.errors = this.errors;

                return (_.keys(this.errors).length === 0);
            },

            check: function(inputName, inputRules) {
                var that = this,
                    parsedRules = this.parse(inputRules),
                    input = inputs.inputs[inputName],
                    value = input.value;

                _.each(_.keys(parsedRules), function(rule) {
                    var validatable = that.validatable(value, input, parsedRules),
                        functionName = 'validate' + rule.charAt(0).toUpperCase() + rule.substring(1).toLowerCase();

                    // Check whether each rule is valid - see if it is validatable and passes tests
                    // Pass the value of the input, the input object, the rule name and the rule parameters
                    if (validatable && !that[functionName](value, input, rule, parsedRules[rule])) {
                        that.error(value, input, rule, parsedRules[rule]);
                    }
                });
            },

            validatable: function(value, input, parsedRules) {
                return this.validateRequired(value, input) || this.implicit(parsedRules);
            },

            implicit: function(parsedRules) {
                return _.has(parsedRules, 'required');
            },

            error: function(value, input, rule, parameters)
            {
                // If singular form is wanted, add the singular message
                var error = rule + ((parameters.value === 'singular') ? '_singular' : '');
                if (!(error in errorMessages)) error = input.type + '_' + rule;
                if (!(error in errorMessages)) return;

                this.errors[input.name] = errorMessages[error];
            },

            validateRequired: function(value, input) {
                return !!((input.type !== 'password') ? value.trim() : value).length;
            },

            validateConfirmed: function(value, input) {
                var confirmed = inputs.inputs[input.name + '_confirmed'];
                if (!confirmed) return false;

                return (confirmed.value === value);
            },

            parse: function(inputRules) {
                var parameters = {},
                    separator = '|',
                    colon = ':';

                if (inputRules.search(separator) === -1) {
                    // There are no separators in the rule - only one rule
                    parameters[inputRules] = false;
                    return parameters;
                }

                _.each(inputRules.split(separator), function(rule) {
                    // Get rule parameters - ex. 'password:singular', where 'singular'
                    // is the parameter

                    var parameter = (rule.search(colon) === -1) ? false : rule.split(colon),
                        key = (parameter) ? parameter[0] : rule,
                        value = (parameter) ? parameter[1] : parameter;

                    parameters[key] = {
                        value : value
                    };
                });

                return parameters;
            }
        };
    };

    var Inputs = function() {
        var object = arguments[0] || false;

        if (!object || _.isUndefined(object.$el)) return false;

        var methods = {
            inputs: {},
            errors: {},

            all: function() {
                var that = this;

                // Clear all inputs
                this.clear();
                // Fill in the inputs array
                _.each(object.$el.find('input[name], textarea[name]'), function(input) {
                    var $input = $(input),
                        name = $input.attr('name'),
                        type = $input.attr('type') || $input.prop('tagName').toLowerCase(),
                        value = $input.val() || $input.text() || '';

                    that.inputs[name] = {
                        name: name,
                        type: type,
                        value: value
                    };
                });

                return this;
            },

            get: function() {
                var input = arguments[0] || false,
                    inputDefault = arguments[1] || false;

                if (!input) return false;
                if (!this.has(input)) return inputDefault;

                return this.inputs[input].value || inputDefault;
            },

            has: function(input) {
                return _.has(this.inputs, input);
            },

            clear: function() {
                this.inputs = {};

                return this;
            },

            validate: function(rules) {
                // Start validating
                this.errors = {};
                this.all();

                return new Validation({
                    inputs: this,
                    rules: rules
                }).valid();
            },

            hasErrors: function() {
                return _.keys(this.errors).length;
            },

            addValidationError: function(inputName, error) {
                this.errors[inputName] = error;
            }
        };

        object.Inputs = methods;

        return methods;
    };

    return Inputs;
});