define([
    'underscore',
    'handlebars',
    'plugins/jquery.transit',

    'avrora'
],

function(_, Handlebars, transit, avrora) {

    var message = {
        /**
         * Catches all kinds of messages from the modules and prints them out
         *
         * @return void
         */
        template: '<div id="{{ identifier }}" data-message="{{ message }}" class="message {{ type }}">' +
                    '<h1>{{ title }}</h1>' +
                    '<p>{{ content }}</p>' +
                    '<a href="#" {{#if speech}} data-speech="{{ speech }}"{{/if}} class="{{ buttonClass }}">{{ button }}</a>' +
                  '</div>',

        add: function(args) {
            var type = ('type' in args) ? args.type : 'alert',
                title = ('title' in args) ? args.title : type,
                content = ('content' in args) ? args.content : '',
                identifier = ('id' in args) ? args.id : 'message-' + Math.floor((Math.random()*1000000)+1),
                speechCommand = ('speech' in args) ? args.speech : false,
                button = ('button' in args) ? args.button : 'Close',
                buttonClass = ('buttonClass' in args) ? args.buttonClass : '',
                inTaskbar = ('inTaskbar' in args && args.inTaskbar) ? true : false,
                persistent = ('persistent' in args && args.persistent) ? true : false,
                messageCount = $('#' + args.id).length,
                that = this;

            if ('id' in args && messageCount && !persistent) {
                return;
            } else if (!messageCount) {
                var template = Handlebars.compile(this.template)({
                    type: type,
                    title: title,
                    content: new Handlebars.SafeString(content),
                    identifier: identifier,
                    speech: speechCommand,
                    button: button,
                    buttonClass: buttonClass
                });

                $(template).prependTo('body');
            }

            var $message = $('#' + identifier);
            if ($message.is(':hidden')) {
                $message.transition({ scale : 0.1 }, 10).show(function() {
                    that.setPositionToAll();
                }).transition({ scale : 1, opacity : 1 });
            }

            if (!messageCount) {
                $message.data('data', {
                    'message' : args.message,
                    'persistent' : persistent
                });

                if (inTaskbar) {
                    var taskbarItems = avrora.modules.get('desktop.collections.taskbaritems');
                    if (!taskbarItems.getByData(args.message).length && args.message) {
                        taskbarItems.add({
                            title : title,
                            data : args.message,
                            type : 'message',
                            icon : 'warning'
                        });
                    }
                }
            }
        },

        remove: function(target, triggered) {
            var $message = ($(target).hasClass('message')) ? $(target) : $(target).parents('.message'),
                data = ($message.length) ? $message.data('data') : false;

            $message.transition({ scale : 0, opacity : 0 }, function() {
                if ((data.persistent && triggered) || !data.persistent) {
                    var taskbarItems = avrora.modules.get('desktop.collections.taskbaritems'),
                        taskbarMessages = taskbarItems.getByData(data.message);
                    if (taskbarMessages.length) taskbarItems.remove(taskbarMessages);

                    $message.remove();
                }
                else $message.hide();
            });
        },

        setPosition: function(message) {
            if (!message.length && $(message).length) message = $(message);
            else return;

            message.css({
                top : ($(document).height() - message.css('height').replace('px', '')*1) / 2,
                left : ($(document).width() - message.css('width').replace('px', '')*1) / 2
            });
        },

        setPositionToAll: function() {
            var $allMessages = $('.message');
            $allMessages.each(function(index) {
                message.setPosition($allMessages.get(index));
            });
        }
    };

    avrora.on('message', function(msg) {
        var args = false;

        try { args = avrora.config.get(msg); }
        catch(e) { args = msg; }

        if (!args) return;

        if (_.isString(msg)) args.message = msg;
        if ('remove' in args) message.remove($('#' + args.id), true);
        else message.add(args);
    });

    $('body').on('click', '.message a', function(e) {
        message.remove(e.target, false);
    });

    $(window).resize(function() {
        message.setPositionToAll();
    });

    return message;
});