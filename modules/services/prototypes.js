define([
    'underscore',
    'handlebars'
],

function(_, Handlebars) {

    var prototypes = {
        prototypeControls: '<div class="prototype-controls grid_24">' +
                           '    <p>The following feature shows demonstrative (forthcoming)<br>and not working functionality.</p>' +
                           '    <p><span>Caution!</span> None of the controls actually work.</p>' +
                           '    <button class="button success">I want to see it!</button>' +
                           '</div>',

        init: function($prototype) {
            var prototypeControls = Handlebars.compile(this.prototypeControls)().trim().replace('\n',''),
                prototypeContents = $prototype.html();

            $prototype.html(prototypeControls + '<div class="prototype-overlay">' + prototypeContents + '</div>');
            $prototype.find('.prototype-controls button').on('click', function() {
                $(this).parents('.prototype').html(prototypeContents);

                return false;
            });

            return this;
        }
    };

    $('body').bind('DOMSubtreeModified', function() {
        var $prototypes = $('body').find('.prototype').not('.parsed');
        if ($prototypes.length) {
            _.each($prototypes, function(prototype) {
                $(prototype).addClass('parsed');
                prototypes.init($(prototype));
            });
        }
    });

    return prototypes;
});