define([
	'underscore',

	'avrora'
],

function(_, avrora) {

	var ping = {
		/**
		 * Constantly checks for connection with the server
		 *
		 * @return void
		 */
		pong: function() {
			var that = this;

			setInterval(function() {
				that.check();
			}, avrora.config.get('application.pingDelay'));

			$(document).on('mousemove touchstart', function() {
				var lastActivity = that.lastActivity || Date.now();
				that.lastActivity = Date.now();
				if (that.lastActivity - lastActivity > avrora.config.get('application.pingDelay')) that.check();
			});

			this.check();
		},

		check: function() {
			if (!document.webkitHidden && avrora.config.get('application.ping')) {
				var baseUrl = avrora.config.get('application.baseURL');
				if (baseUrl !== window.location.origin + '/my') {
					baseUrl = window.location.origin + '/my';
				}

				$.get(baseUrl + '/ping')
					.success(function() { avrora.trigger('pingSuccess'); })
					.error(function() { avrora.trigger('pingError'); });
			}
		}
	};

	// Ping-pong
	//ping.pong();

	return ping;
});