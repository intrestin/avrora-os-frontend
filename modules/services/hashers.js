define([
    'underscore',
    'plugins/sha1',
    'plugins/md5',

    'avrora'
],

function(_, sha1, md5, avrora) {

    var hashers = {
        salt: function() {
            return (((1+Math.random())*0x10000)|0).toString(16).substring(1);
        },

        md5: function(string) {
            return CryptoJS.MD5(string).toString();
        },

        sha1: function(string) {
            return CryptoJS.SHA1(string).toString(CryptoJS.enc.Hex);
        },

        saltedSha1: function(string) {
            return this.sha1(this.salt() + string + this.salt());
        }
    };

    return hashers;
});