define([
    'backbone',

    'avrora',

    'modules/desktop/models/taskbar-item'
],

function(Backbone, avrora, TaskbarItemModel) {

    avrora.namespace.collections.TaskbarItems = Backbone.Collection.extend({
        model: TaskbarItemModel,

        getByData: function(data) {
            return this.filter(function(item) {
                return item.get('data') === data;
            });
        },

        getByTitle: function(title, insensitive) {
            return this.filter(function(app) {
                if (!insensitive) return app.get('title') === title;
                else {
                    var appTitle = app.get('title').toUpperCase();
                    title = title.toUpperCase();

                    return (appTitle === title || appTitle.search(title) !== -1);
                }
            });
        }
    });

    return avrora.namespace.collections.TaskbarItems;
});
