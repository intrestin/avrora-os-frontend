define([
    'backbone',

    'avrora',

    'modules/desktop/models/active-app'
],

function(Backbone, avrora, ActiveAppModel) {

    avrora.namespace.collections.ActiveApps = Backbone.Collection.extend({
        model: ActiveAppModel,

        getAll: function() {
            return this.filter(function(app) {
                return app.get('application').id;
            });
        },

        getActive: function() {
            return this.filter(function(app){
                return app.get('active');
            });
        },

        getWasActive: function() {
            return this.filter(function(app){
                return app.get('was_active');
            });
        },

        getByIdentifier: function(id) {
            return this.filter(function(app) {
                return app.get('identifier') === id;
            });
        },

        getByUrl: function(url) {
            return this.filter(function(app) {
                return app.get('application').url === url;
            });
        },

        getByState: function(state) {
            return this.filter(function(app) {
                return app.get('state') === state;
            });
        },

        getAllBuiltin: function() {
            return this.filter(function(app) {
                return app.get('builtin');
            });
        }
    });

    return avrora.namespace.collections.ActiveApps;
});
