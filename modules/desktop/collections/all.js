/**
 * Avrora OS autoloader for the module's collections
 */

define([
	'underscore',

	// Module models
	'modules/desktop/collections/applications',
    'modules/desktop/collections/active-apps',
    'modules/desktop/collections/taskbar-items',
    'modules/desktop/collections/feedback-entries'
],

function(_, Applications, ActiveApps, TaskbarItems, FeedbackEntries) {
	var collections = {
		applications: Applications,
		activeapps: ActiveApps,
        taskbaritems: TaskbarItems,
        feedbackentries: FeedbackEntries
	};

	return collections;
});