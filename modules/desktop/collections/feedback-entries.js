define([
    'backbone',

    'avrora',

    'modules/desktop/models/feedback'
],

function(Backbone, avrora, FeedbackModel) {

    avrora.namespace.collections.FeedbackEntries = Backbone.Collection.extend({
        model: FeedbackModel,
        url: avrora.config.get('application.api') + '/feedback/'
    });
    return avrora.namespace.collections.FeedbackEntries;
});