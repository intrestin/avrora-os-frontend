define([
    'backbone',

	'avrora',

    'modules/desktop/models/application'
],

function(Backbone, avrora, ApplicationModel) {

    avrora.namespace.collections.Applications = Backbone.Collection.extend({
        model: ApplicationModel,
		url: avrora.config.get('application.api') + '/application/',

        getApplicationByUri: function(uri) {
            return this.filter(function(app) {
                return app.url() === uri;
            });
        },

        getApplicationByUrl: function(url) {
            return this.filter(function(app) {
                return app.get('url') === url;
            });
        },

        getByName: function(name, insensitive) {
            return this.filter(function(app) {
                if (!app.get('id')) return;
                if (!insensitive) return app.get('name') === name;
                else {
                    var appName = app.get('name').toUpperCase();
                    name = name.toUpperCase();

                    return (appName === name || appName.search(name) !== -1);
                }
            });
        }
    });

    return avrora.namespace.collections.Applications;
});
