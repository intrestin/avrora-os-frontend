define([
	'backbone',
    'plugins/sha1',

	'avrora'
],

function(Backbone, sha1, avrora) {

	avrora.namespace.models.ActiveApp = Backbone.Model.extend({
        defaults: {
            'identifier' : false,
            'builtin' : false,
            'application' : false,
            'opened' : false,
            'active' : false,
            'was_active' : false,
            'state' : 0,
            'previous_state' : 0,
            'fullscreen' : false,
            'minimized' : false,
            'hidden' : false
        },

        initialize: function() {
            if (!this.get('identifier') && this.get('application')) {
                var identifier = avrora.services.hashers.sha1(this.get('application').url + this.get('application').name);
                return this.set('identifier', identifier);
            }

            return this;
        },

        activate: function() {
            return this.set('active', true);
        },

        deactivate: function() {
            return this.set('active', false);
        },

        open: function() {
            return this.set('opened', true);
        },

        close: function() {
            return this.set('opened', false);
        },

        state: function(state) {
            this.activate();
            return this.set('state', state);
        },

        isActive: function() {
            return this.get('active');
        },

        wasActive: function() {
            return this.get('was_active');
        },

        isMaximized: function() {
            return this.get('state') === 4;
        },

        isMinimized: function() {
            return this.get('minimized');
        },

        isHidden: function() {
            return this.get('hidden');
        },

        isFullscreen: function() {
            return this.get('state') === 5;
        }
	});

	return avrora.namespace.models.ActiveApp;
});