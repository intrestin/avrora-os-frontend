define([
	'backbone',

	'avrora'
],

function(Backbone, avrora) {

	avrora.namespace.models.TaskbarItem = Backbone.Model.extend({
        defaults: {
            'placement' : 'taskbar'
        }
	});

	return avrora.namespace.models.TaskbarItem;
});