define([
    'backbone',

    'avrora'
],

function(Backbone, avrora) {

    avrora.namespace.models.FeedbackEntry = Backbone.RelationalModel.extend({
        urlRoot: avrora.config.get('application.api') + '/feedback/'
     });

    return avrora.namespace.models.FeedbackEntry;
});