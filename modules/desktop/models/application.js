define([
	'backbone',

	'avrora'
],

function(Backbone, avrora) {

	avrora.namespace.models.Application = Backbone.RelationalModel.extend({
		urlRoot: avrora.config.get('application.api') + '/application/'
	});

	return avrora.namespace.models.Application;
});