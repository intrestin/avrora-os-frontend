/**
 * Avrora OS autoloader for the module's models
 */

define([
	'underscore',

	// Module models
	'modules/desktop/models/application',
    'modules/desktop/models/active-app',
    'modules/desktop/models/taskbar-item',
    'modules/desktop/models/feedback'
],

function(_, Application, ActiveApp, TaskbarItem, FeedbackEntry) {
	var models = {
		application: Application,
		activeapp: ActiveApp,
        taskbaritem: TaskbarItem,
        feedback: FeedbackEntry
	};

	return models;
});