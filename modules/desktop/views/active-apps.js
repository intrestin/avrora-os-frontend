define([
    'jquery',
    'underscore',
    'backbone',
    'handlebars',

    'avrora',

    // Templates
    'text!modules/desktop/templates/active-apps.html'
],

function($, _, Backbone, Handlebars, avrora, activeAppsTemplate) {

    avrora.namespace.views.ActiveAppsView = Backbone.View.extend({
        el: $('#active-apps'),

        events: {
            'click .active-app a' : 'clickActiveApp',
            'mouseover'           : 'mouseoverActiveApps',
            'mouseout'            : 'mouseoutActiveApps'
        },

        initialize: function() {
            var that = this;
            this.activeStateState = false;
            this.hoverStateState = false;
            this.triggeredStateState = false;
            this.manualClick = false;

            this
                .on('show', function() {
                    that.showActiveApps();
                })
                .on('hide', function() {
                    that.hideActiveApps();
                })
                .on('activateApp', function(app) {
                    that.activateActiveApp(app);
                })
                .on('deactivateApp', function(app) {
                    that.deactivateActiveApp(app);
                });

            this.collection
                .on('reset', function() {
                    that.render();
                })
                .on('remove', function(app) {
                    that.removeActiveApp(app);
                });

            $(window)
                .on('resize', function() {
                    that.resizeActiveApps();
                });

            $(document)
                .click(function() {
                    if (that.activeState && !that.triggeredState) that.trigger('hide');
                });
        },

        remove: function () {
            $(window).off('resize');
        },

        render: function() {
            this.$el.html(this.renderContent());

            return this;
        },

        renderContent: function() {
            var context = {
                    applications: this.collection.toJSON()
                },
                content = Handlebars.compile(activeAppsTemplate)(context);

            return content.trim().replace('\n','');
        },

        /* View events */

        clickActiveApp: function(e) {
            e.preventDefault();
            this.manualClick = true;

            var $target = $(e.target);
            if (!$target.hasClass('active-app')) $target = $target.parents('.active-app');

            var identifier = this.getIdentifier($target),
                app = this.collection.getByIdentifier(identifier);

            if (app.length) app = app[0];
            else return;
            app.activate();
        },

        mouseoverActiveApps: function() {
            var that = this;

            this.hoverState = true;
            if (this.triggeredState) clearTimeout(this.triggeredState);
            setTimeout(function() {
                if (that.hoverState && !that.activeState) that.trigger('show');
            }, 100);
        },

        mouseoutActiveApps: function() {
            var that = this;

            this.hoverState = false;
            setTimeout(function() {
                if (!that.hoverState && that.activeState) that.trigger('hide');
            }, 3000);
        },

        /* View methods */

        showActiveApps: function() {
            this.activeState = true;
            this.$el.find('.inner-container').show().stop(true, true).animate({'margin-left': 0, opacity: 1});

            if (avrora.modules.get('desktop.views.applicationmanager').isActiveApplicationFullscreen()) {
                avrora.modules.get('desktop.views.taskbar').showTaskbar();
            }
        },

        hideActiveApps: function() {
            var that = this;
            this.$el.find('.inner-container').stop(true, true).animate({'margin-left': '-55px', opacity: 0}, function() {
                that.$el.find('.inner-container').hide();
                avrora.modules.get('desktop.views.taskbar').$el.removeClass('more-zindex');
                that.activeState = false;
            });

            if (avrora.modules.get('desktop.views.applicationmanager').isActiveApplicationFullscreen()) {
                avrora.modules.get('desktop.views.taskbar').hideTaskbar();
            }
        },

        activateActiveApp: function(app) {
            var that = this,
                $activeApp = this.getActiveAppContainer(this.$el, app).find('a');

            this.$el.find('li a span').css('background-color', 'transparent');
            $activeApp.find('span').css('background-color', '');
            $activeApp.find('i').removeClass('dark');
            $activeApp.addClass('active');

            if (that.triggeredState) clearTimeout(this.triggeredState);
            this.trigger('show');
            this.triggeredState = setTimeout(function() {
                that.trigger('hide');
                that.triggeredState = false;
            }, 2000);
        },

        deactivateActiveApp: function(app) {
            var $appContainer = this.getActiveAppContainer(this.$el, app);
            $appContainer.find('a').removeClass('active');
            $appContainer.find('i').addClass('dark');
        },

        removeActiveApp: function(app) {
            this.getActiveAppContainer(this.$el, app).remove();
            $(window).resize();
        },

        resizeActiveApps: function() {
            var documentHeight = $(document).height(),
                $activeIcons = this.$el.find('ul li span'),
                activeIconsCount = $activeIcons.length;

            var activeIconsHeight = 54,
                paddingTop = (documentHeight - activeIconsCount * activeIconsHeight)/2,
                $ulElement = this.$el.find('ul'),
                minHeight = documentHeight - 2*activeIconsHeight;

            $ulElement.css({
                'max-height' : minHeight + 'px',
                'min-height' : minHeight + 'px'
            });

            if (paddingTop < activeIconsHeight) paddingTop = activeIconsHeight;

            this.$el.css({ 'padding-top' : paddingTop + 'px' });

            if ($ulElement.find('.active').length && !this.manualClick) {
                var scrollTop = $ulElement.scrollTop() + $ulElement.find('.active').position().top - $ulElement.height()/2 + $ulElement.find('.active').height()/2;

                $ulElement.scrollTop(scrollTop);
            }
            this.manualClick = false;
        },

        getActiveAppContainer: function($dom, app) {
            return $dom.find('#activeapp-' + app.get('identifier'));
        },

        getIdentifier: function($activeApp) {
            if (!$activeApp.attr('id')) return false;

            return $activeApp.attr('id').replace('activeapp-', '');
        }
    });

    return avrora.namespace.views.ActiveAppsView;
});
