define([
    'jquery',
    'underscore',
    'backbone',
    'handlebars',

    'avrora',

    // Templates
    'text!modules/desktop/templates/applications.html',
    'text!modules/desktop/templates/modal-app.html'
],

function($, _, Backbone, Handlebars, avrora, applicationsTemplate, modalAppTemplate) {

    Handlebars.registerHelper('ifStateIs', function(state, options) {
        if (this.state === state) return options.fn(this);
        else return options.inverse(this);
    });

    avrora.namespace.views.ApplicationManagerView = Backbone.View.extend({
        el: $('#applications'),

        events: {
            'click .app-button.close'       : 'closeApplication',
            'click .app-button.maximize'    : 'maximizeApplication',
            'click .app-button.minimize'    : 'minimizeApplication',
            'click .app-button.state-right' : 'stateRight',
            'click .app-button.state-left'  : 'stateLeft',
            'click .app-button.state-up'    : 'stateUp',
            'click .app'                    : 'focusApplication'
        },

        // Prepare the application manager
        initialize: function() {
            var that = this;
            this.activeAppsView = avrora.modules.get('desktop.views.activeapps');
            this.activeAppsOrder = [];
            this.desktopShown = false;
            this.canShowDesktop = true;

            this
                .on('openApplication', function(app) {
                    that.openApplication(app, false);
                });

            this.collection
                .on('add', function(app) {
                    that.addApplication(app);
                })
                .on('reset', function() {
                    that.render();
                })
                .on('remove', function(app) {
                    that.removeApplicationContainer(app);
                });

            avrora.applications
                .on('openBuiltinApp', function(app) {
                    that.openApplication(app, true);
                })
                .on('openModalApp', function(app) {
                    that.openModalApplication(app);
                })
                .on('uninstalledApp', function(app) {
                    that.collection.remove(that.collection.getByUrl(app.get('url')));
                });

            $(window)
                .on('resize', function() {
                    that.resizeApplicationContainers();
                    that.resizeModalApplication();
                })
                .on('blur', function() {
                    $('iframe').filter(function() {
                        return $(this).data('mouseIn');
                    }).trigger('iframeclick');
                });

            $(document)
                .on('mousemove', function(e) {
                    if ((e.pageX + e.pageY + 6) >= ($(document).width() + $(document).height())) that.showDesktop();
                })
                .on('mouseenter', 'iframe', function() {
                    $(this).data('mouseIn', true);
                })
                .on('mouseout', 'iframe', function() {
                    $(this).data('mouseIn', false);
                });

            $('body')
                .on('click', function(e) {
                    var $target = $(e.target);

                    if ($target.length && !$target.hasClass('modal-app') && !$target.parents('.modal-app').length) {
                        that.removeModalApplications();
                    }
                });
        },

        // Render the content
        render: function() {
            this.$el.html(this.renderContent());

            return this;
        },

        // Get the content
        renderContent: function() {
            var context = {
                    applications: this.collection.toJSON()
                },
                content = Handlebars.compile(applicationsTemplate)(context);

            return content.trim().replace('\n','');
        },

        /* View events */

        // Closes the application triggered by the close button
        closeApplication: function(e) {
            var $app = $(e.target).parents('.app'),
                app = this.collection.getByIdentifier(this.getIdentifier($app));

            if (app.length) {
                this.removeApplication(app[0]);
            }
        },

        // Change application's state to up(4)
        // Maximizes the application
        maximizeApplication: function(e) {
            var $app = $(e.target).parents('.app'),
                app = this.collection.getByIdentifier(this.getIdentifier($app));

            this.toggleMaximize(app);
        },

        // Minimizes the application
        minimizeApplication: function(e) {
            var $app = $(e.target).parents('.app'),
                app = this.collection.getByIdentifier(this.getIdentifier($app));

            this.toggleMinimize(app);
        },

        // Change application's state to up(3)
        stateRight: function(e) {
            var $app = $(e.target).parents('.app'),
                app = this.collection.getByIdentifier(this.getIdentifier($app));

            this.toggleApplicationState(3, app);
        },

        // Change application's state to up(2)
        stateLeft: function(e) {
            var $app = $(e.target).parents('.app'),
                app = this.collection.getByIdentifier(this.getIdentifier($app));

            this.toggleApplicationState(2, app);
        },

        // Change application's state to up(1)
        stateUp: function(e) {
            var $app = $(e.target).parents('.app'),
                app = this.collection.getByIdentifier(this.getIdentifier($app));

            this.toggleApplicationState(1, app);
        },

        // Focus an application by clicking on its container
        focusApplication: function(e) {
            var $target = $(e.target);
            if (!$target.hasClass('app')) $target = $(e.target).parents('.app');

            var application = this.collection.getByIdentifier(this.getIdentifier($target));
            if (application.length && !application[0].isMinimized()) application[0].activate();
        },

        /* View methods */

        // This method is triggered by:
        //      openApp(application, builtin=false)
        //      openBuiltinApp(application, builtin=true)
        // Adds the application to the collection
        openApplication: function(app, builtin) {
            builtin = builtin || false;

            // Gets prevously added application with this url
            var existingApp = this.collection.getByUrl((builtin) ? '#/' + app.config.url : app.get('url'));

            // If any exist, activate it
            if (existingApp.length) {
                existingApp[0].activate();
            } else {
                // The application is new

                // The application data
                var application = false;
                if (builtin) {
                    // If the app is builtin, create the app data
                    application = {
                        name : app.config.name,
                        url : '#/' + app.config.url,
                        icon : app.config.icon,
                        view : app.getView('main'),
                        rendered : false,
                        id : this.collection.length
                    };

                    // If no view is selected, then the app is not valid
                    if (!application.view) return;
                } else {
                    // The application is a normal one, get its attributes
                    application = app.attributes;
                }

                // Check if the app data is invalid
                if (!application) return;

                // Add the application to the active apps collection.
                this.collection.add(avrora.modules.get('desktop').model('activeapp', {
                    application : application,
                    builtin : builtin
                }));
            }
        },

        // This method is triggered by:
        //      openModalApp(application)
        // Opens a model application
        openModalApplication: function(app) {
            var that = this;

            // Only one at a time
            if (this.$el.find('.modal-app.new').length) return;
            if (this.$el.find('.modal-app.' + app.config.url).length) return;
            this.removeModalApplications();

            // Get modal app container
            var modalApp = Handlebars.compile(modalAppTemplate)().trim().replace('\n','');

            // Add modal app element to DOM
            this.$el.append(modalApp);

            var $modalApp = this.$el.find('.modal-app').addClass('new').addClass(app.config.url);

            // Set dimensions and position
            var dimensions = {
                    width: app.config.modal.width || 300,
                    height: app.config.modal.height || 150
                };
            $modalApp
                .css(dimensions)
                .find('.app-inner')
                .css('max-height', app.config.modal.maxheight || dimensions.height);

            // Render content
            var appView = app.view();
            appView.setElement($modalApp.find('.app-content')).render();

            // Show modal app
            $modalApp.transition({scale: 0}, 5, function() {
                $modalApp.show();
                that.resizeModalApplication();

                if ('afterRender' in appView) appView.afterRender();
                $modalApp.transition({scale: 1}, 500);
            });

            _.delay(function() {
                $modalApp.removeClass('new');
            }, 1000);
        },

        // This method is triggered by the event:
        //      this.collection.add(application)
        // Binds essential events to app's model
        addApplication: function(app) {
            var that = this;

            // Apply on-change events
            // and open the application
            app
                .off('change:active').off('change:opened')
                .off('change:minimized').off('change:state')
                .off('change:hidden')
                .on('change:active', function(model, active) {
                    if (active) that.activateApplication(model);
                    else that.deactivateApplication(model);
                })
                .on('change:opened', function(model, opened) {
                    if (opened) that.createApplicationContainer(model);
                    else that.removeApplicationContainer(model);
                })
                .on('change:minimized', function(model, minimized){
                    if (!minimized) that.showApplication(model, false);
                    else that.hideApplication(model, false);
                })
                .on('change:hidden', function(model, hidden){
                    if (!hidden) that.showApplication(model, true);
                    else that.hideApplication(model, true);
                })
                .on('change:state', function(model) {
                    that.resizeApplicationContainer(that.getAppContainer(that.$el, model), true);
                })
                .open();
        },

        // This method is triggered by change:opened (activeapp) event
        // Creates app's container and activates the application
        createApplicationContainer: function(model) {
            // Render application's placeholders - render and get the HTML only
            var $content = $('<div>' + this.renderContent() + '</div>'),
                $activeAppsContent = $(this.activeAppsView.renderContent());

            if (!$content) return;

            // Get application's container and add it to applications placeholder
            this.$el.append(this.getAppContainer($content, model).addClass('animate'));

            // The same is done for the active apps placeholder
            var $activeApp = this.activeAppsView.getActiveAppContainer($activeAppsContent, model);
            this.activeAppsView.$el.find('ul').append($activeApp);

            // Make the application active
            model.activate();
        },

        // Removes application container from DOM
        removeApplicationContainer: function(app) {
            this.getAppContainer(this.$el, app).remove();
        },

        // This method is triggered by change:active (activeapp) event
        activateApplication: function(model) {
            // Get application's container
            var $app = this.getAppContainer(this.$el, model);

            // If this is the currently active app, abort
            if (model.previous('active') === model.isActive()) return;

            // Deactivate all other active apps
            this.deactivateApplications(model);
            this.removeModalApplications();

            // Activate the app in the active apps container
            this.activeAppsView.trigger('activateApp', model);

            if (model.get('builtin') && !model.get('application').rendered) {
                // If the app is builtin and is not rendered yet
                try {
                    // Render it
                    model.get('application').view.setElement($app.find('.app-content')).render();
                    model.get('application').rendered = true;
                } catch(e) {
                    // The render has failed
                    avrora.log('An error occurred while rendering ' + model.get('name') + ': ' + e);
                    this.collection.remove(model);
                    return;
                }
            } else if (!model.get('builtin') && !$app.find('iframe').hasClass('loaded')) {
                // The app is not builtin
                // Start loading the iframe
                $app.find('iframe').hide().load(function() {
                    $app.find('.avrora-loading').fadeOut('fast', function() {
                        $(this).remove();
                        $app.find('iframe').addClass('loaded').fadeIn('fast');
                    });
                }).on('iframeclick', function() {
                    model.activate();
                });
            }

            $(window).resize();

            // Add to order list
            this.activeAppsOrder = _.without(this.activeAppsOrder, model.get('identifier'));
            this.activeAppsOrder.push(model.get('identifier'));

            // Change the z-indexes
            this.setZindexes();

            // Make it visible that the window is actually active
            this.$el.find('> .app').addClass('unfocused-app');
            $app.removeClass('unfocused-app');

            // If the app is in fullscreen mode
            if (model.get('state') === 5) {
                if (model.isActive()) $app.addClass('more-zindex');
                $app.find('.app-topbar').css('opacity', 0);
            }

            if ($app.hasClass('animate')) {
                this.$el.find('.animate').removeClass('animate');
                $app.transition({scale : 0}, 5, function() {
                    $(this).show().transition({scale : 1}, 500);
                    if (model.get('builtin') && 'afterRender' in model.get('application').view) appView.afterRender();
                });
            }
        },

        // This method is triggered by change:active (activeapp) event
        deactivateApplication: function(model) {
            this.activeAppsView.trigger('deactivateApp', model);
        },

        // This method is triggered by change:minimized/change:hidden (activeapp) event
        showApplication: function(model) {
            var $app = this.getAppContainer(this.$el, model);

            if (model.wasActive()) {
                model.set('was_active', false);
                model.activate();
            }

            $app.show().transition({scale : 1}, 500);
        },

        // This method is triggered by change:minimized/change:hidden (activeapp) event
        hideApplication: function(model, hideOnly) {
            var $app = this.getAppContainer(this.$el, model);

            if (model.isActive()) {
                if (hideOnly) model.set('was_active', true);
                model.deactivate();
            }

            $app.transition({scale: 0}, 200, function() {
                $app.hide();
            });
        },


        // This method removes an application from the collection
        removeApplication: function(app) {
            app = app || this.collection.getActive();
            if (app.length) app = app[0];

            var that = this,
                $app = this.getAppContainer(this.$el, app);

            $app.transition({scale: 0}, 200, function() {
                // The remove action triggers remove event which removes
                // the app from DOM
                that.collection.remove(app);
            });
        },

        // Removes a model application from DOM
        removeModalApplication: function($app) {
            if ($app.hasClass('new')) return;

            $app.transition({scale: 0}, 200, function() {
                $app.remove();
            });
        },

        // A convenient method for toggleing application's state
        toggleApplicationState: function(state, app, notoggle) {
            app = app || this.collection.getActive();
            if (app.length) app = app[0];

            if (app.get('state') !== state || notoggle) app.state(state);
            else app.state(0);
        },

        // Convenient method for maximizing
        toggleMaximize: function(app) {
            app = app || this.collection.getActive();
            if (app.length) app = app[0];

            if (!app.isMaximized()) {
                app.set('previous_state', app.get('state'));
                app.state(4);
            } else {
                app.state(app.get('previous_state'));
            }
        },

        // Convenient method for minimizing
        toggleMinimize: function(app) {
            app = app || this.collection.getActive();
            if (app.length) app = app[0];

            if (!app.isMinimized()) {
                app.set('minimized', true);
            } else {
                app.set('minimized', false);
            }
        },

        // Convenient method for activating/deactivating app's fullscreen mode
        toggleFullscreen: function(app) {
            app = app || this.collection.getActive();
            if (app.length) app = app[0];

            if (!app.isFullscreen()) {
                app.set('fullscreen', app.get('state'));
                app.state(5);
            } else {
                app.state(app.get('fullscreen'));
                this.setZindexes();
            }
        },

        // Checks whether the currently active app is fullscreen
        isActiveApplicationFullscreen: function() {
            var activeApp = this.collection.getActive();

            if (activeApp.length) {
                activeApp = activeApp[0];

                return (activeApp.isFullscreen());
            }

            return false;
        },


        // Remove all modal applications
        removeModalApplications: function() {
            var that = this;

            // Only one at a time
            if (this.$el.find('.modal-app').length) {
                _.each(this.$el.find('.modal-app'), function(modalApp) {
                    that.removeModalApplication($(modalApp));
                });
            }
        },

        // Deactivate all applications
        deactivateApplications: function(activatedApp) {
            var applications = this.collection.getAll(),
                that = this;

            _.each(applications, function(app) {
                if (!activatedApp) {
                    // Show the desktop, hide all applications
                    if (!app.isMinimized()) app.set('hidden', that.desktopShown);
                } else {
                    // Reset minimized and was active values
                    if (app.wasActive() || app.isHidden()) {
                        app.set({
                            'was_active' : false,
                            'hidden' : false
                        });
                    }

                    // Deactivate all except the one which has been just activated
                    if (activatedApp.get('identifier') !== app.get('identifier')) app.deactivate();
                    else if (app.isMinimized()) app.set('minimized', false);
                }
            });
        },

        // Resize a single application container
        resizeApplicationContainer: function($app, animated) {
            var application = this.collection.getByIdentifier(this.getIdentifier($app)),
                state = 0,
                taskbar = avrora.modules.get('desktop.views.taskbar'),
                taskbarWidth = (taskbar.autohide) ? 0 : taskbar.$el.width(),
                width = $(document).width() - taskbarWidth,
                height = $(document).height() / 2,
                placement = 'bottom',
                position = 0,
                coords = false,
                defaultCoords = { x : 0, y : height };

            animated = animated || false;

            if (!application.length) return;
            application = application[0];
            state = application.get('state');

            $app.css({
                'top' : 'auto',
                'right' : 'auto',
                'bottom' : 'auto',
                'left' : 'auto'
            });

            $app.find('.maximize i').attr('class', 'icon icon-maximize');
            $app.find('.state-left i').attr('class', 'icon icon-left');
            $app.find('.state-up i').attr('class', 'icon icon-up');
            $app.find('.state-right i').attr('class', 'icon icon-right');
            $app.find('.app-topbar').css('opacity', 1);

            switch(state) {
            case 1:
                $app.find('.state-up i').attr('class', 'icon icon-down');
                height -= 5;
                placement = 'top';
                coords = { x : 0, y : 0 };
                break;
            case 2:
                $app.find('.state-left i').attr('class', 'icon icon-down');
                width /=  2;
                height *= 2;
                placement = 'left';
                coords = { x : 0, y : 0 };
                break;
            case 3:
                $app.find('.state-right i').attr('class', 'icon icon-down');
                width /=  2;
                height *= 2;
                placement = 'right';
                position = taskbarWidth;
                coords = { x : width, y : 0 };
                break;
            case 4:
                $app.find('.maximize i').attr('class', 'icon icon-minimize');
                height = $(document).height();
                coords = { x : 0, y : 0 };
                break;
            case 5:
                $app.addClass('more-zindex');
                $app.find('.app-topbar').css('opacity', 0);
                width = $(document).width();
                height = $(document).height();
                coords = { x : 0, y : 0 };
                break;
            }

            var properties = {
                'width' : width,
                'height' : height
            };

            if (!coords) coords = defaultCoords;

            if (!animated) {
                properties[placement] = position + 'px';
                return $app.css(properties);
            } else {
                $app.css($app.data('coords') || defaultCoords).css('opacity', 0.9);

                return $app.stop().animate(_.extend(properties, coords, {'opacity' : 1}), 350, function() {
                    $app.data('coords', coords);
                    $app.css(placement, position + 'px').css('transform', '');
                });
            }
        },

        // Auto-resize all application containers
        // Triggered by window resize event
        resizeApplicationContainers: function() {
            var $apps = this.$el.find('.app'),
                that = this;

            if (!$apps.length) return;

            $apps.each(function() {
                that.resizeApplicationContainer($(this), false);
            });
        },

        // Auto-position and resize modal applications
        resizeModalApplication: function() {
            var $modalApp = this.$el.find('.modal-app');

            if (!$modalApp.length) return;

            $modalApp.each(function() {
                var position = {
                    'top': ($(document).height() - $(this).height())/2,
                    'left': ($(document).width() - $(this).width())/2
                };

                $(this).css(position);
            });
        },

        // Set z-indexes for all application containers
        setZindexes: function() {
            var $apps = this.$el.find('.app'),
                that = this;

            if (!$apps.length) return;

            $apps.each(function() {
                var zindex = 1800 + _.indexOf(that.activeAppsOrder, that.getIdentifier($(this)));
                $(this).removeClass('more-zindex').css('z-index', zindex);

                if ($(this).is(':hidden')) {
                    $(this).show();
                }
            });
        },

        // A convenient method for showing the desktop
        showDesktop: function() {
            var that = this;

            if (this.canShowDesktop) {
                this.canShowDesktop = false;
                this.desktopShown = !this.desktopShown;
                this.deactivateApplications(false);
                setTimeout(function() {
                    that.canShowDesktop = true;
                }, 700);
            }
        },

        // Switch between apps using keyboard shortcut
        switchBetweenApps: function() {
            var $activeApps = this.activeAppsView.$el.find('li'),
                $currentActive = $activeApps.find('.active');

            if (!$activeApps.length) return;

            var tabbingIterator = $currentActive.parents('li').index() - 1;
            if (tabbingIterator < 0) tabbingIterator = $activeApps.length - 1;

            $($activeApps.get(tabbingIterator)).find('a').click();
        },


        // A helper method for getting application container from DOM
        getAppContainer: function($dom, app) {
            var $app = $dom.find('#app-' + app.get('identifier'));

            if ($app.length) return $app;
            return false;
        },

        // Fetch the app's identifier from its container
        getIdentifier: function($app) {
            if (!$app.attr('id')) return false;

            return $app.attr('id').replace('app-', '');
        }
    });

    return avrora.namespace.views.ApplicationManagerView;
});
