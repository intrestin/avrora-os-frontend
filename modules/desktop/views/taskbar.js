define([
    'jquery',
    'underscore',
    'backbone',
    'handlebars',
    'plugins/jquery.transit',
    'plugins/jquery.mousewheel',
    'plugins/moment',

    'avrora',

    // Templates
    'text!modules/desktop/templates/taskbar.html'
],


function($, _, Backbone, Handlebars, transit, mousewheel, moment, avrora, desktopTaskbarTemplate) {

    // A handlebars helpers for rendering taskbar and toolbar items
    Handlebars.registerHelper('taskbarItems', function() {
        var items = this.items,
            itemsCount = items.length,

            out = '';

        for (var j=0; j<itemsCount; j++)
        {
            var item = items[j];
            if (_.isUndefined(item) || _.isUndefined(item.type) || item.placement !== 'taskbar') continue;

            out += '<li data-tooltip="' + item.title + '" data-' + item.type + '="' + item.data + '" data-placement="left">';
            out += '<i data-speech="' + item.speech + '" class="icon icon-' + item.icon + ' medium"></i>';
            out += '</li>';
        }

        return out;
    });

    Handlebars.registerHelper('toolbarItems', function() {
        var items = this.items,
            itemsCount = items.length,

            out = '';

        for (var j=0; j<itemsCount; j++)
        {
            var item = items[j];
            if (_.isUndefined(item) || _.isUndefined(item.type) || item.placement !== 'toolbar') continue;

            out += '<li data-tooltip="' + item.title + '" data-' + item.type + '="' + item.data + '" data-placement="left">';
            out += '<i class="icon icon-' + item.icon + '"></i>';
            out += '</li>';
        }

        return out;
    });

    avrora.namespace.views.DesktopTaskbarView = Backbone.View.extend({
        el: $('#taskbar'),

        events: {
            'click li[data-message]' : 'openMessage',
            'click li[data-application]' : 'openApplication',
            'click li[data-itemlink]' : 'triggerItem',
            'click li[data-item="desktop"]' : 'showDesktop',
            'click li[data-feedback]' : 'renderFeedbackDialog'
        },

        initialize: function() {
            var that = this;
            this.autohide = false;
            this.taskbarHotcorner = false;

            this.collection
                .on('add', function() {
                    that.render();
                })
                .on('remove', function() {
                    that.render();
                });

            avrora
                .on('pingSuccess', function() {
                    that.signalChange(true);
                })
                .on('pingError', function() {
                    that.signalChange(false);
                });

            $(window)
                .on('resize', function() {
                    that.resizeTaskbar();
                });

            $(document)
                .on('mousemove', function(e) {
                    that.activateHotcorner(e);
                });

            this.$el
                .bind('mousewheel', function() {
                    return false;
                });

            this.collection.reset();
            _.each(avrora.applications.getAllItems(), function(app) {
                that.collection.add({
                    title : app.config.name,
                    data : app.config.url,
                    type : 'application',
                    icon : app.config.icon,
                    speech : app.config.speech,
                    placement : app.config.placement
                }, {silent: true});
            });
            this.collection.add(avrora.config.get('taskbar'), {silent: true});
        },

        remove: function () {
            $(window).off('resize');
        },

        render: function() {
            var context = {
                    items: this.collection.toJSON()
                },
                content = Handlebars.compile(desktopTaskbarTemplate)(context);

            this.$el.html(content);
            $(window).resize();

            return this;
        },

        renderFeedbackDialog: function() {
            this.FeedbackView = avrora.modules.get('desktop').view('feedback', {
                model: avrora.modules.get('desktop.models.feedback'),
                collection: avrora.modules.get('desktop.collections.feedbackentries')
            }).render();
        },

        /* View events */

        openMessage: function(e) {
            var $warning = $(e.target);
            if (!$warning.attr('data-message')) $warning = $warning.parents('li[data-message]');

            try {
                var message = $warning.data('message');
                avrora.trigger('message', avrora.config.get(message));
            } catch(er) {}
        },

        openApplication: function(e) {
            var $app = $(e.target);
            if (!$app.data('application')) $app = $app.parents('li[data-application]');

            try {
                var url = $app.data('application');
                avrora.applications.openByUrl(url);
            } catch(er) {}
        },

        triggerItem: function(e) {
            e.preventDefault();

            var $item = $(e.target);
            if (!$item.data('itemlink')) $item = $item.parents('li[data-itemlink]');

            var url = '#/' + $item.data('itemlink');
            avrora.router.navigate(url, {trigger: true});
        },

        showDesktop: function(e) {
            e.preventDefault();
            avrora.modules.get('desktop.views.applicationmanager').showDesktop();
        },

        /* View methods */

        showTaskbar: function(onload) {
            var isActiveAppFullscreen = avrora.modules.get('desktop.views.applicationmanager').isActiveApplicationFullscreen();

            if (onload || isActiveAppFullscreen || this.autohide) {
                var $taskbar = this.$el,
                    taskbarWidth = $taskbar.width();

                if (onload) this.clock();
                else if (isActiveAppFullscreen) $taskbar.addClass('more-zindex');

                $taskbar
                    .css('right', '-' + taskbarWidth + 'px')
                    .show()
                    .addClass('visible');

                if ((onload && !this.autohide) || (!onload && this.autohide) || isActiveAppFullscreen) {
                    $taskbar.stop().animate({ 'right' : 0 });
                }
            }
        },

        hideTaskbar: function() {
            var that = this,
                $taskbar = this.$el,
                taskbarWidth = $taskbar.width();

            $taskbar
                .stop()
                .animate({'right' : '-' + taskbarWidth + 'px'}, function() {
                    if (!that.autohide) {
                        $taskbar
                            .css({ 'right' : 0 })
                            .removeClass('more-zindex')
                            .removeClass('visible');
                    }
                });
        },

        activateHotcorner: function(e) {
            var taskbarWidth = this.$el.width(),
                isHidden = this.autohide || avrora.modules.get('desktop.views.applicationmanager').isActiveApplicationFullscreen(),
                dimensionsLimit = ((e.pageX >= $(document).width() - 6) && (e.pageY <= $(document).height() - 6));

            if (dimensionsLimit && !this.taskbarHotcorner && isHidden && !this.$el.hasClass('visible')) {
                this.taskbarHotcorner = true;
                this.showTaskbar();
            } else if ((e.pageX <= ($(document).width() - taskbarWidth)) && this.taskbarHotcorner) {
                this.taskbarHotcorner = false;
                this.hideTaskbar();
            }
        },

        signalChange: function(signal) {
            var $signal = this.$el.find('[data-item="signal"]');

            if (signal) {
                $signal.removeClass('no-signal');

                if (avrora.offline) {
                    avrora.offline = false;
                    avrora.trigger('message', {
                        remove: true,
                        id: 'message-offline'
                    });
                }
            } else {
                $signal.addClass('no-signal');

                if (!avrora.offline) {
                    avrora.offline = true;
                    avrora.trigger('message', 'messages.offline');
                }
            }
        },

        resizeTaskbar: function() {
            if (this.$el.is(':hidden')) return;
            var $taskbarElement = this.$el;

            // Calculate taskbar's apps height
            var appsHeight = $(document).height() - $taskbarElement.find('.datetime-holder').height() - $taskbarElement.find('.toolbar-holder').height();
            $taskbarElement.find('.apps-holder').css('height', appsHeight + 'px');

            // Add to this height other values like margins
            var $appHolders = $taskbarElement.find('.apps-holder').find('li'),
                $appHolder = $appHolders.first(),
                numberOfApps = $appHolders.length,
                appHeight = $appHolder.height(),
                margin = $appHolder.css('margin-top').replace('px', '') * 2,
                padding = $appHolder.css('padding-top').replace('px', '') * 2,
                realAppsHeight = (appHeight + margin + padding) * numberOfApps;

            // Calculate apps' top padding
            var appsPaddingTop = (appsHeight - realAppsHeight) / 2;

            if (realAppsHeight > appsHeight) {
                // Small icons
                $taskbarElement.find('.apps-holder').data('small-height', realAppsHeight);
                $taskbarElement.find('.apps-holder').find('li').removeClass().addClass('small');
                $taskbarElement.find('.apps-holder').find('li .icon').removeClass('medium').addClass('small');
                $taskbarElement.css('min-height', $taskbarElement.find('.apps-holder').height() + $taskbarElement.find('.datetime-holder').height() + $taskbarElement.find('.toolbar-holder').height() - 70);
            } else {
                var smallHeight = $taskbarElement.find('.apps-holder').data('small-height');

                if (smallHeight < appsHeight) {
                    $taskbarElement.find('.apps-holder').find('li').removeClass();
                    $taskbarElement.find('.apps-holder').find('li .icon').removeClass('small').addClass('medium');
                }
            }

            if (appsPaddingTop < -10) appsPaddingTop = -10;

            $taskbarElement.find('.apps-holder').find('ul').css('padding-top', appsPaddingTop + 'px');
        },

        clock: function() {
            var dateString = moment().format('DD/MM') + '<br>' + moment().format('YYYY'),
                timeString = moment().format('H:mm'),
                $datetimeElement = this.$el.find('.datetime'),
                $dateElement = $datetimeElement.find('.date'),
                $timeElement = $datetimeElement.find('.time');

            $dateElement.html(dateString);
            $timeElement.html(timeString);

            var that = this;
            _.delay(function() {
                that.clock();
            }, 1000);
        }
    });

    return avrora.namespace.views.DesktopTaskbarView;
});
