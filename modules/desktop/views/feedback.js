define([
    'jquery',
    'underscore',
    'backbone',
    'handlebars',
    'plugins/jquery.imgareaselect',
    'plugins/jquery.select',
    'plugins/dropzone-amd-module',

    'avrora',

    'modules/desktop/models/feedback',

    // Templates
    'text!modules/desktop/templates/feedback.html'
],

function($, _, Backbone, Handlebars, imgSelect, selectInput, dropzone, avrora, Feedback, feedbackDialogTemplate) {
    avrora.namespace.views.FeedbackView = Backbone.View.extend({
        el: $("body"),

        events: {
            'click #make-screenshot' : 'selectScreenshotArea',
            'click button[name=send-feedback]' : 'submitFeedbackEntry',
            'click a.close-feedback-modal' : 'closeModal',
            'click .overlay' : 'closeModal'
        },

        initialize: function() {
            var that = this;
            this.screenshotPreviewArea = $(".screenshot-preview");

            $(window)
                .on('resize', function() {
                    that.setCentered();
                });
        },

        render: function() {
            var context = {},
                dialog = Handlebars.compile(feedbackDialogTemplate)(context),
                that = this;

            this.$el.append(dialog);
            $("select#feedback_type").customSelect();

            this.setCentered();
            that.showModal();
            $(".drop-zone").dropzone({
                url:'feedback/screenshot/upload',
                clickable:'.choose-file',
                enqueueForUpload: false,
                previewsContainer: '.screenshot-preview',
                previewTemplate: '<div class="preview file-preview"></div>',
                addedfile: function(file) {
                    return true;
                },
                processingfile: function(file) {
                    return true;
                },
                thumbnail: function(file) {
                    var fileReader = new FileReader();
                    var thumbnail = $('<img />');
                    thumbnail.css({
                        'max-width' : '350px',
                        'max-height' : '150px'
                    });
                    fileReader.onloadend = function(event) {
                        thumbnail.attr('src', event.target.result);
                        $("#screenshot-url-input").val(event.target.result);
                        $(".screenshot-preview").html(thumbnail);
                        _.defer(function() {
                            that.setCentered();
                        });
                    };
                    fileReader.readAsDataURL(file);
                }
            });
        },

        showModal: function() {
            $('.overlay').show();
            $('.modal-window').show();
            $('#desktop-container').css({
                '-webkit-filter': 'blur(1px)'
            });
        },

        hideModal: function() {
            $('.modal-window').hide();
            $('.overlay').hide();
            $('#desktop-container').css({
                '-webkit-filter': 'blur(0px)'
            });
        },

        removeModal: function() {
            $('.modal-window').remove();
            $('.overlay').remove();
            this.undelegateEvents();
        },

        closeModal: function() {
            this.hideModal();
            this.removeModal();
        },

        selectScreenshotArea: function() {
            var that = this,
                captureBtn = '<a href="#" id="capture-screenshot" class="tiny inner button">Capture</a>';
            this.hideModal();
            $("#desktop-container").imgAreaSelect({
                x1: 0,
                y1:0,
                x2:100,
                y2:100,
                fadeSpeed: 300,
                onSelectChange: function(elem, selection) {
                    $("#capture-screenshot").hide();
                },
                onSelectEnd: function(element, selection) {
                    setTimeout( function() {
                        $("#capture-screenshot").fadeIn('fast');
                    }, 500);
                    $("#capture-screenshot").on('click', function() {
                        that.renderScreenshot(element, selection);
                    });
                }
            });
            $(".imgareaselect-border3").html(captureBtn);
        },

        renderScreenshot: function(element, selection) {
            var that = this;
            $(element).data('imgAreaSelect').cancelSelection();

            html2canvas(element, {
                onrendered: function(canvas) {
                    console.log("Rendered! Cropping...");
                    var image = new Image(),
                        cropped = new Image(),
                        capture = document.createElement('canvas'),
                        context = capture.getContext('2d');
                    image.onload = function() {
                        capture.width = selection.width;
                        capture.height = selection.height;
                        context.drawImage(image, selection.x1, selection.y1,
                            selection.width, selection.height, 0, 0,
                            selection.width, selection.height);

                        cropped = $(cropped).attr('src', capture.toDataURL());
                        cropped.css({
                            'max-height' : 150,
                            'max-width': 350
                        });
                        $(".screenshot-preview").html(cropped);
                        $("#screenshot-url-input").val(cropped.attr('src'));
                        _.defer( function() {
                            that.setCentered();
                        });
                        that.showModal();
                    };
                    image.src = canvas.toDataURL();
                }
            });
        },

        submitFeedbackEntry: function(e) {
            e.preventDefault();
            var form = $(e.target).parents('form'),
                that = this;
            this.collection.create({
                'message': form.find('#feedback_message').val(),
                'type': form.find("#feedback_type").val(),
                'screenshot': form.find("#screenshot-url-input").val()
            });
            form.hide().remove();
            $("#feedback-modal").find('.success-msg').fadeIn(300, function() {
                that.setCentered();
            });
        },

        getModalSize: function() {
            var modal = $(".modal-window"),
                modalSize = { height: modal.height(), width: modal.width() };

            var modalPadding = {
                top: parseInt(modal.css('padding-top'), 10),
                right: parseInt(modal.css('padding-right'), 10),
                bottom: parseInt(modal.css('padding-bottom'), 10),
                left: parseInt(modal.css('padding-left'), 10)
            };

            return {
                height: modalSize.height + modalPadding.top + modalPadding.bottom,
                width: modalSize.width + modalPadding.left + modalPadding.right
            };
        },

        setCentered: function() {
            var page = $("#desktop-container"),
                dialog = $("#feedback-modal"),
                pageSize = { width: page.width(), height: page.height() },
                modalSize = this.getModalSize();

            dialog.stop();
            dialog.animate({
                top: (pageSize.height - modalSize.height) / 2,
                left: (pageSize.width - modalSize.width) / 2
            }, 500);
        }

    });

    return avrora.namespace.views.FeedbackView;
});
