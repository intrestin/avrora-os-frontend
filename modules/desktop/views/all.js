/**
 * Avrora OS autoloader for the module's views
 */

define([
	'underscore',

	// Module views
	'modules/desktop/views/main',
	'modules/desktop/views/launcher',
	'modules/desktop/views/taskbar',
	'modules/desktop/views/feedback',
	'modules/desktop/views/active-apps',
	'modules/desktop/views/application-manager'
],

function(_, MainView, LauncherView, TaskbarView, FeedbackView, ActiveAppsView, ApplicationManagerView) {

	var views = {
		main: MainView,
		launcher: LauncherView,
		taskbar: TaskbarView,
		feedback: FeedbackView,
		activeapps: ActiveAppsView,
		applicationmanager: ApplicationManagerView
	};

	return views;
});