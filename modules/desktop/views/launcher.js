define([
    'jquery',
    'underscore',
    'backbone',
    'plugins/jquery.sortable',
    'plugins/jquery.transit',
    'plugins/hammer',
    'plugins/jquery.mousewheel',
    'handlebars',

    'avrora',

    // Templates
    'text!modules/desktop/templates/launcher.html'
],

function($, _, Backbone, sortable, transit, hammer, mousewheel, Handlebars, avrora, launcherTemplate) {
    // A handlebars helper for rendering all applications on the desktop
    Handlebars.registerHelper('applications', function() {
        var applications = this.applications,
            applicationCount = applications.length,
            numberOfSlides = applicationCount / 20,

            out = '',
            currentApplication = 1;
        for (var i=0; i<numberOfSlides; i++)
        {
            out += '<ul class="apps' + ((i === 0) ? ' visible' : '') + '">';

            for (var j=currentApplication; j<20 + currentApplication; j++)
            {
                var application = applications[j-1];

                if (_.isUndefined(application)) continue;

                out += '<li>';
                out += '<div data-url="' + application.url + '">';
                var icon = (application.icon_url) ? application.icon_url : application.icon;
                out += '<img src="' + icon + '" alt="' + application.name + '" />';
                out += '<h4>' + application.name + '</h4>';
                out += '</div>';
                out += '</li>';
            }
            currentApplication += 20;

            out += '</ul>';
        }

        return out;
    });

    // A handlebars helper for rendering the drawer pagination
    Handlebars.registerHelper('drawerPagination', function() {
        var applications = this.applications,
            applicationCount = applications.length,
            numberOfSlides = applicationCount / 20;

        if (numberOfSlides <= 1) return '';

        var out = '<ul class="pagination">';

        for (var i=0; i<numberOfSlides; i++)
        {
            var dataPage = i + 1;
            out += '<li ' + ((i === 0) ? 'class="active"' : '') + ' data-page="' + dataPage + '"><span></span></li>';
        }

        out += '</ul>';

        return out;
    });

    avrora.namespace.views.LauncherView = Backbone.View.extend({
        el: $('#desktop-icons'),

        events: {
            'click .application-drawer ul.pagination li' : 'changeHomescreenPage',
            'click .application-drawer ul.apps li'       : 'openApplication'
        },

        initialize: function() {
            var that = this;

            this
                .on('changeHomescreen', function(direction) {
                    that.changeHomescreen(direction);
                });

            this.collection.get('applications')
                .on('add', function(app) {
                    that.addApplicationToHomescreen(app);
                    that.prepareLauncher();
                })
                .on('remove', function(app) {
                    that.removeApplicationFromHomescreen(app);
                });

            $(window)
                .on('resize', function() {
                    that.resizeLauncher();
                });

            this.$el.bind('mousewheel', function(event, delta) {
                if (Math.abs(delta) >= 1) that.trigger('changeHomescreen', (delta > 0) ? 'right' : 'left');
                return false;
            });
        },

        remove: function () {
            $(window).off('resize');
        },

        render: function() {
            this.$el.html(this.renderContent());

            this.prepareLauncher();

            return this;
        },

        renderContent: function() {
            var context = {
                applications: this.collection.get('applications').toJSON()
            };
            var content = Handlebars.compile(launcherTemplate)(context);

            return content.trim().replace('\n','');
        },

        /* View events */

        changeHomescreenPage: function(e) {
            var $applicationDrawer = this.$el.find('.application-drawer');

            var $target = $(e.target);
            if ($target.hasClass('active')) return false;

            var newHomescreenNumber = $target.attr('data-page'),
                appDrawerWidth = $applicationDrawer.width(),
                offsetSize = appDrawerWidth * (newHomescreenNumber - 1);

            $applicationDrawer.find('.homescreens').animate({'left' : '-' + offsetSize + 'px'});

            $target.parent().find('.active').removeClass('active');
            $target.addClass('active');
        },

        openApplication: function(e) {
            var $target = $(e.target);

            var url = $target.find('div').attr('data-url');
            if (_.isUndefined(url)) url = $target.attr('data-url');
            if (_.isUndefined(url)) url = $target.parent().attr('data-url');

            var app = this.collection.get('applications').where({ 'url' : url });
            if (app.length) app = app[0]; else return;

            avrora.modules.get('desktop.views.applicationmanager').trigger('openApplication', app);
        },

        /* View methods */

        prepareLauncher: function() {
            var that = this;

            // Swipe left or right
            var hammer = new Hammer(this.$el.get(0));
            hammer.onswipe = function(ev) {
                if ($(ev.originalEvent.target).parents('ul.apps').length) return;

                that.trigger('changeHomescreen', ev.direction);
            };

            try
            {
                // Init sortable
                this.$el.find('ul.apps').sortable('destroy');
                this.$el.find('ul.apps').sortable({
                    connectWith: 'ul.apps',
                    placeholderTagName: 'li',
                    uniqueIdAttr: 'data-url',
                    interactWith: [
                        {
                            element: avrora.modules.get('desktop.views.taskbar').$el.find('li[data-item="trash"]'),
                            onDrop: function(applicationUrl, element) {
                                element.removeClass('active');
                                var application = that.collection.getUserAppBy('url', applicationUrl);
                                if (!application) return;
                                that.collection.uninstallApplication(application);
                            },
                            onDragover: function(element) {
                                element.addClass('active');
                            },
                            onDragenter: function(element) {
                                element.addClass('active');
                            },
                            onDragleave: function(element) {
                                element.removeClass('active');
                            }
                        }
                    ]
                });
            }
            catch (e) {}

            $(window).resize();
        },

        changeHomescreen: function(direction) {
            var $applicationDrawer = this.$el.find('.application-drawer'),
                $pagination = $applicationDrawer.find('ul.pagination'),
                changingHomescreen = 0;

            if (changingHomescreen === 0) {
                changingHomescreen++;

                var currentHomescreenNumber = $pagination.find('.active').attr('data-page') * 1,
                    newHomescreenNumber = (direction === 'left') ?
                        currentHomescreenNumber + 1 : ((direction === 'right') ? currentHomescreenNumber - 1 : 0),
                    $newHomescreen = $pagination.find('li[data-page="' + newHomescreenNumber + '"]');

                if ($newHomescreen.length) {
                    $newHomescreen.click();
                }

                setTimeout(function() {
                    changingHomescreen = 0;
                }, 500);
            }
        },

        addApplicationToHomescreen: function(app) {
            if (this.getAppIcon(this.$el, app)) return;

            var $applications = $(this.renderContent()),
                $app = this.getAppIcon($applications, app),
                $homescreens = this.$el.find('.homescreens .apps');

            if (!$homescreens.length) this.render();
            else {
                $app.transition({ scale : 0.1, opacity : 0 }, 5);
                $homescreens.last().append($app);

                if ($homescreens.last().find('li').length > 20) this.render();

                $app.stop().transition({ scale : 1, opacity : 1 }, 300, function() {
                    $(window).resize();
                });
            }
        },

        removeApplicationFromHomescreen: function(app) {
            var $app = this.getAppIcon(this.$el, app),
                $homescreens = this.$el.find('.homescreens .apps'),
                that = this;

            $app.stop().transition({ scale : 0.1, opacity : 0 }, 300, function() {
                $app.remove();
                if ($homescreens.find('li').length === ($homescreens.length - 1) * 20) that.render();
                $(window).resize();
            });
        },

        getAppIcon: function($dom, app) {
            var $app = $dom.find('div[data-url="' + app.get('url') + '"]').parents('li');

            if ($app.length) return $app;
            return false;
        },

        resizeLauncher: function() {
            this.calculateWidth();

            // Get elements
            var $applicationDrawer = $('#desktop-container').find('.application-drawer'),
                $desktopIconsHolder = this.$el,
                $desktopIcons = $applicationDrawer.find('div.active').find('ul.apps').find('li'),
                $pagination = $applicationDrawer.find('div.active').find('ul.pagination');

            var numberOfIcons = $desktopIcons.length;
            if (numberOfIcons <= 0) return;

            // Set proper left position of the homescreens
            var homescreen = $applicationDrawer.find('ul.pagination .active').attr('data-page') * 1,
                appDrawerWidth = $applicationDrawer.width(),
                offsetSize = appDrawerWidth * (homescreen - 1);
            if (homescreen !== 1)
                $applicationDrawer.find('.homescreens').css({'left' : '-' + offsetSize + 'px'});

            // Calculate the drawer's height
            var desktopHeight = $('#desktop-container').height(),
                appDrawerMarginTop = $applicationDrawer.css('margin-top').replace('px', '') * 1,
                appDrawerHolderMaxHeight = desktopHeight - 2*appDrawerMarginTop;

            var iconSize = $desktopIcons.first().width(),
                iconMargin = $desktopIcons.first().css('margin-bottom').replace('px', '') * 1,
                minHeight = 4 * (iconSize + iconMargin);

            if (appDrawerHolderMaxHeight < minHeight) appDrawerHolderMaxHeight = minHeight;

            // Apply the new height to the drawer
            $applicationDrawer.find('ul.apps').css({ 'height' : appDrawerHolderMaxHeight + 'px', 'max-height' : appDrawerHolderMaxHeight + 'px' });
            $applicationDrawer.css({ 'height' : appDrawerHolderMaxHeight + 'px', 'max-height' : appDrawerHolderMaxHeight + 'px' });

            // Get the max number of grid elements - x and y
            var iconGridElementsX = (numberOfIcons > 5) ? 5 : numberOfIcons; // @TODO: more flexible;
            //var iconGridElementsY = (numberOfIcons >= 5*4) ? 4 : Math.ceil(numberOfIcons / 5); // more flexible;

            // Calculate and apply the new icon size
            var newIconSize1 = ($desktopIconsHolder.width() - 2*(iconSize+4*iconMargin)) / iconGridElementsX - iconMargin - 10,
                newIconSize2 = appDrawerHolderMaxHeight / 4 - iconMargin,
                newIconSize = (newIconSize1 + newIconSize2) / 2;

            if (newIconSize < 64) newIconSize = 64;
            else if (newIconSize > 120) newIconSize = 120;

            $desktopIcons.css({ 'height' : newIconSize + 'px', 'width' : newIconSize + 'px'});
            var marginAndTextSize = newIconSize / 8;
            $desktopIcons.find('img').css('height', 0.5 * newIconSize + 'px').css('margin', marginAndTextSize - 3 + 'px 0px');
            if (marginAndTextSize < 11) marginAndTextSize = 11;
            $desktopIcons.find('h4').css({'font-size': marginAndTextSize + 'px', 'line-height' : marginAndTextSize + 7 + 'px'});

            // Calculate and apply drawer's width
            iconSize = (_.isUndefined(newIconSize)) ? iconSize : newIconSize;
            var appDrawerHolderMaxWidth = iconGridElementsX * (iconSize + iconMargin + 4);
            if (iconGridElementsX > 5) appDrawerHolderMaxHeight += 20;
            $pagination.css('width', appDrawerHolderMaxWidth + 'px');
            $applicationDrawer.css({ 'width' : appDrawerHolderMaxWidth + 'px', 'max-width' : appDrawerHolderMaxWidth + 'px', 'min-width' : appDrawerHolderMaxWidth + 'px' });
            $applicationDrawer.find('.homescreens').css('width', appDrawerHolderMaxWidth*$applicationDrawer.find('ul.apps').length + 'px');
            $applicationDrawer.find('.homescreens').find('ul.apps').css('width', appDrawerHolderMaxWidth + 'px');
        },

        calculateWidth: function() {
            var taskbar = avrora.modules.get('desktop.views.taskbar'),
                taskbarWidth = taskbar.$el.width();

            if (taskbar.autohide) taskbarWidth = 0;

            this.$el.css('width', $(document).width() - taskbarWidth);
        }
    });

    return avrora.namespace.views.LauncherView;
});
