define([
    'jquery',
    'underscore',
    'backbone',
    'handlebars',
    'plugins/jquery.preload',

    'avrora',

    // Templates
    'text!modules/desktop/templates/main.html'
],

function($, _, Backbone, Handlebars, preload, avrora, desktopTemplate) {

    avrora.namespace.views.DesktopView = Backbone.View.extend({
        el: $('body'),

        initialize: function() {
            var that = this;

            avrora.account.currentUser
                .on('change:wallpaper', function(model) {
                    that.setWallpaper(model.get('wallpaper'));
                });
        },

        render: function() {
            this.$el.html(Handlebars.compile(desktopTemplate));

            this.$el.addClass('white');
            if ($('#welcome-text').length > 1) $('#welcome-text').get(0).remove();
            $('#welcome-text').show();
            $('#desktop-container').hide();

            this.renderDesktopViews();
            this.preloadImages();

            return this;
        },

        renderDesktopViews: function() {
            // Load the taskbar
            this.TaskbarView = avrora.modules.get('desktop').view('taskbar', {
                el: this.$el.find('#taskbar'),
                model: avrora.modules.get('desktop.models.taskbaritem'),
                collection: avrora.modules.get('desktop.collections.taskbaritems')
            }).render();

            // Load the launcher
            this.LauncherView = avrora.modules.get('desktop').view('launcher', {
                el: this.$el.find('#desktop-icons'),
                model: this.model,
                collection: avrora.account.currentUser
            }).render();

            // Load the active apps
            this.ActiveAppsView = avrora.modules.get('desktop').view('activeapps', {
                el: this.$el.find('#active-apps'),
                model: avrora.modules.get('desktop.models.activeapp'),
                collection: avrora.modules.get('desktop.collections.activeapps')
            }).render();

            // Load the application manager
            this.AppManagerView = avrora.modules.get('desktop').view('applicationmanager', {
                el: this.$el.find('#applications'),
                model: avrora.modules.get('desktop.models.activeapp'),
                collection: avrora.modules.get('desktop.collections.activeapps')
            }).render();
        },

        preloadImages: function() {
            // Get the files that need preloading
            var that = this,
                desktopWallpaper = this.model.get('wallpaper'),
                preloadImages = [
                    desktopWallpaper,
                    'assets/images/icons/big.png',
                    'assets/images/icons/medium.png',
                    'assets/images/icons/small.png'
                ];

            this.LauncherView.$el.find('img').each(function() {
                preloadImages.push($(this).attr('src'));
            });

            // Preload them
            $.preload(
                preloadImages,
                {
                    onFinish: function() {
                        // Change the desktop wallpaper
                        if (!_.isUndefined(desktopWallpaper) && !_.isNull(desktopWallpaper)) {
                            that.setWallpaper(desktopWallpaper);
                        }

                        that.loadDesktop();
                    }
                }
            );
        },

        loadDesktop: function() {
            var that = this;

            $(window).resize();

            // Remove any unwanted elements
            $('#welcome-text').remove();

            $('#desktop-container').transition({ scale : 0.1, opacity : 0 }, 5, function() {
                $(this).show();

                $(this).transition({ scale : 1, opacity : 1 }, 800, function() {
                    that.TaskbarView.showTaskbar(true);

                    $(window).resize();
                    that.$el.removeClass('white');
                });
            });
        },

        setWallpaper: function(wallpaper) {
            var $desktop = $('#desktop-container #desktop-icons');
            if (wallpaper[0] === '#') {
                $desktop.css('background', wallpaper);
            } else {
                $desktop
                    .css('background', 'url(' + wallpaper + ') no-repeat center center fixed')
                    .css({
                        '-webkit-background-size' : 'cover',
                        '-moz-background-size' : 'cover',
                        '-o-background-size' : 'cover',
                        'background-size' : 'cover'
                    });
            }
        }
    });

    return avrora.namespace.views.DesktopView;
});
