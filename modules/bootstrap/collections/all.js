/**
 * Avrora OS autoloader for the module's collections
 */

define([
	'underscore',

	// Module models
	'modules/bootstrap/collections/bootstraps'
],

function(_, BootstrapsCollection) {
	var collections = {
		bootstraps: BootstrapsCollection
	};

	return collections;
});