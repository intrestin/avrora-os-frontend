define([
    'backbone',

    'avrora'
],

function(Backbone, avrora) {

    avrora.namespace.collections.BootstrapsCollection = Backbone.Collection.extend({
        initialize: function() {
        }
    });

    return avrora.namespace.collections.BootstrapsCollection;
});
