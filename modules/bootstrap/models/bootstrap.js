define([
	'backbone',

	'avrora'
],

function(Backbone, avrora) {

	avrora.namespace.models.BootstrapModel = Backbone.Model.extend({
		initialize: function() {
		}
	});

	return avrora.namespace.models.BootstrapModel;
});