/**
 * Avrora OS autoloader for the module's models
 */

define([
	'underscore',

	// Module models
	'modules/bootstrap/models/bootstrap'
],

function(_, BootstrapModel) {
	var models = {
		bootstrap: BootstrapModel
	};

	return models;
});