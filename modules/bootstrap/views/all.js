/**
 * Avrora OS autoloader for the module's views
 */

define([
	'underscore',

	// Module views
	'modules/bootstrap/views/main'
],

function(_, MainView) {
	var views = {
		main: MainView
	};

	return views;
});