define([
    'jquery',
    'underscore',
    'backbone',

    'avrora',
    
    // Templates
    'text!modules/bootstrap/templates/main.html'
],

function($, _, Backbone, avrora, bootstrapTemplate) {

    avrora.namespace.views.BootstrapView = Backbone.View.extend({
        el: $('body'),

        render: function() {
            this.$el.html(bootstrapTemplate);
        }
    });

    return avrora.namespace.views.BootstrapView;
});
