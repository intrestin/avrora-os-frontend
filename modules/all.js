/**
 * Avrora OS autoloader for the modules
 */

define([
    'underscore'
],

function(_) {
    return {
        /**
         * Determine if a module is already loaded
         *
         * @param  string  module
         * @return bool
         */
        has: function(module) {
            return _.has(this, module);
        },

        /**
         * Get a module.
         *
         * If no module is requested or does not exist, the function will return false.
         *
         * @param  string   module
         * @param  function callback
         * @return mixed
         */
        get: function(module, callback) {
            var noCallback = false;
            if (_.isUndefined(module)) return false;
            if (_.isUndefined(callback)) noCallback = true;

            var moduleParts = module.split('.'),
                modulePartsLength = moduleParts.length;

            if (modulePartsLength > 1) {
                // The wanted module is something like auth.models.account

                // Get the module name
                var moduleName = moduleParts[0];

                // Only prevously loaded modules can use the above pattern
                if (this.has(moduleName)) {
                    module = this[moduleName];
                    var resourceType = (modulePartsLength > 1) ? moduleParts[1] : null,
                        resourceName = (modulePartsLength > 2) ? moduleParts[2] : null;

                    if (!_.isNull(resourceType) && !_.isNull(resourceName)) {
                        var resource = null;

                        switch(resourceType) {
                        case 'models':
                            resource = module.model(resourceName);
                            break;
                        case 'collections':
                            resource = module.collection(resourceName);
                            break;
                        case 'views':
                            resource = module.view(resourceName);
                            break;
                        }

                        return resource;
                    }
                } else {
                    return false;
                }
            }

            if (this.has(module)) {
                // The module has been already loaded
                return (noCallback) ? this[module] : callback(this[module]);
            } else {
                return false;
            }
        },

        /**
         * Set a module.
         *
         * @param  string  module
         * @param  object  moduleObject
         * @return void
         */
        set: function(module, moduleObject, loadingModules) {
            if (_.isUndefined(module)) return false;
            if (_.isUndefined(loadingModules)) loadingModules = false;

            if (loadingModules) this[module] = moduleObject;
            else this.applications[module] = moduleObject;

            require('avrora').log(((loadingModules) ? 'Module' : 'Built-in application') + ' ' + module + ' has been set.');
        },

        /**
         * Load all installed modules.
         *
         * @param  array  installedModules
         * @param  function  callback
         * @return void
         */
        load: function(installedModules, builtinApplications, callback) {
            if (callback === null) return false;

            try {
                var that = this,
                    moduleCount = installedModules.length,
                    appsCount = builtinApplications.length;
                if (moduleCount === 0) return false;

                // Get all required files for preloading
                var modulesFiles = [],
                    models = [],
                    collections = [],
                    views = [],
                    config = [],
                    applicationsFiles = [],
                    services = [];

                for (var iterator = 0; iterator < moduleCount; iterator++) {
                    var module = installedModules[iterator];
                    models.push('modules/' + module + '/models/all');
                    collections.push('modules/' + module + '/collections/all');
                    views.push('modules/' + module + '/views/all');
                }
                modulesFiles = _.union(models, collections, views);

                models = [],
                collections = [],
                views = [],
                config = [];
                for (var appIterator = 0; appIterator < appsCount; appIterator++) {
                    var application = builtinApplications[appIterator];
                    models.push('modules/applications/' + application + '/models');
                    collections.push('modules/applications/' + application + '/collections');
                    views.push('modules/applications/' + application + '/views');
                    config.push('modules/applications/' + application + '/config');
                }
                applicationsFiles = _.union(models, collections, views, config);

                services.push('modules/services/all');
                var reqJs = _.union(modulesFiles, applicationsFiles, services);

                require(reqJs, function() {
                    var loadedModules = 0,
                        loadedApps = 0,
                        loadingModules = true,
                        module;

                    // Set modules and applications
                    for (var iterator = 0; iterator < (moduleCount + appsCount); iterator++) {
                        loadingModules = (iterator < moduleCount);

                        module = (loadingModules) ? installedModules[loadedModules++] : builtinApplications[loadedApps++];

                        // Module's data
                        var count = (loadingModules) ? iterator : 3*moduleCount + loadedApps-1,
                            allCount = ((loadingModules) ? moduleCount : appsCount),
                            moduleData = {
                                moduleName: module,
                                models: arguments[count],
                                collections: arguments[count + allCount],
                                views: arguments[count + allCount * 2]
                            };

                        if (!loadingModules) {
                            var config = arguments[count + allCount * 3],
                                defaultConfig = {
                                    name: module,
                                    url: config.uid || module,
                                    icon: 'empty',
                                    speech: '',
                                    placement: 'none',
                                    modal: false
                                };
                            moduleData.config = _.extend(defaultConfig, config);
                        }

                        moduleData = _.extend(moduleData, {
                            // Model's/collection's objects are put here
                            data: {},
                            // View's objects are put here
                            ui: {},

                            /**
                             * Append the module name
                             *
                             * @param  string str
                             * @return string
                             */
                            appendModuleName: function(str) {
                                return this.moduleName + '.' + str;
                            },

                            /**
                             * Get module's model.
                             *
                             * @param  string modelName
                             * @return object, bool
                             */
                            model: function(modelName, args) {
                                if (_.isUndefined(modelName)) return false;
                                if (_.isUndefined(args)) args = false;
                                if (!_.has(this.models, modelName)) return false;

                                if (args) {
                                    return new this.models[modelName]((args && args !== true) ? args : {});
                                }

                                return this.models[modelName];
                            },
                            getModel: function(modelName, args) {
                                return this.model(modelName, args);
                            },

                            /**
                             * Get module's collection.
                             *
                             * @param  string collectionName
                             * @return object, bool
                             */
                            collection: function(collectionName, args) {
                                if (_.isUndefined(collectionName)) return false;
                                if (_.isUndefined(args)) args = false;
                                if (!_.has(this.collections, collectionName)) return false;

                                if (!_.has(this.data, collectionName)) {
                                    this.data[collectionName] = new this.collections[collectionName](args ? args : {});
                                }
                                return this.data[collectionName];
                            },
                            getCollection: function(collectionName, args) {
                                return this.collection(collectionName, args);
                            },

                            /**
                             * Get module's view.
                             *
                             * @param  string viewName
                             * @return object, bool
                             */
                            view: function(viewName, args) {
                                if (_.isUndefined(viewName)) viewName = 'main';
                                if (_.isUndefined(args)) args = false;
                                if (!_.has(this.views, viewName)) return false;

                                if (!_.has(this.ui, viewName) || args) {
                                    // @TODO More flexibility
                                    require('avrora').log(this.appendModuleName(viewName) + ' is loaded');
                                    this.ui[viewName] = new this.views[viewName](args ? args : {});
                                }

                                return this.ui[viewName];
                            },
                            getView: function(viewName, args) {
                                return this.view(viewName, args);
                            }
                        });

                        that.set(module, moduleData, loadingModules);

                        if ((moduleCount + appsCount) === (loadedModules + loadedApps)) callback();
                    }

                    // Set services
                    _.extend(that.services, arguments[arguments.length - 1]);
                });
            } catch(e) {
                return false;
            }
        },

        /**
         * The containing object for the built-in applications
         */
        applications: {
            open: function(name, modalApp) {
                if (!_.has(this, name)) return false;
                this.trigger(((!modalApp) ? 'openBuiltinApp' : 'openModalApp'), this[name]);
            },

            openByUrl: function(url) {
                var app = this.getByUrl(url);
                if (!app) return;
                app = app[0];

                this.open(app.moduleName, app.config.modal);
            },

            getAllItems: function() {
                return _.filter(this, function(app) {
                    return (!_.isUndefined(app.config));
                });
            },

            getByUrl: function(url) {
                return _.filter(this.getAllItems(), function(app) {
                    return app.config.url === url;
                });
            },

            getTaskbarItems: function() {
                return _.filter(this.getAllItems(), function(app) {
                    return app.config.placement === 'taskbar';
                });
            },

            getToolbarItems: function() {
                return _.filter(this.getAllItems(), function(app) {
                    return app.config.placement === 'toolbar';
                });
            },

            getLauncherItems: function() {
                return _.filter(this.getAllItems(), function(app) {
                    return app.config.placement === 'launcher';
                });
            }
        },

        /**
         * Services placeholder
         */
        services: {}
    };
});