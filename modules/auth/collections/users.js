define([
    'backbone',

    'avrora',

    'modules/auth/models/user'
],

function(Backbone, avrora, AccountUserModel){

    avrora.namespace.collections.AccountUsers = Backbone.Collection.extend({
        model: AccountUserModel,
		url: avrora.config.get('application.api') + '/account-user/',

        createUser: function(data) {
            var name = data.name,
                emailHash = avrora.services.hashers.md5(avrora.account.get('user').email),
                avatar = data.avatar || 'http://www.gravatar.com/avatar/' + emailHash + '?s=300',
                passwordHash = avrora.services.hashers.sha1(data.password);

            this.create({
                name: name,
                password: passwordHash,
                avatar: avatar,
                wallpaper: avrora.config.get('assets.defaultWallpaper')
            }, { wait : true });
        }
    });

    return avrora.namespace.collections.AccountUsers;
});
