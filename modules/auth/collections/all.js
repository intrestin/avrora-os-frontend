/**
 * Avrora OS autoloader for the module's collections
 */

define([
	'underscore',

	// Module collections
	'modules/auth/collections/users'
],

function(_, AccountUsers) {

	var collections = {
		users: AccountUsers
	};

	return collections;
});