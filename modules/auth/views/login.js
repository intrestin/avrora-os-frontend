define([
    'jquery',
    'underscore',
    'backbone',
    'handlebars',
    'plugins/jquery.transit',
    'plugins/jquery.preload',
    'plugins/sha1',

    'avrora',

    // Templates
    'text!modules/auth/templates/login.html'
],

function($, _, Backbone, Handlebars, transit, preload, sha1, avrora, loginTemplate) {

    avrora.namespace.views.LoginView = Backbone.View.extend({
        el: $('body'),

        events: {
            'click #users #avatars a.avatar' : 'openLoginBox',
            'click .close-btn, .user-name'   : 'closeLoginBox',
            'click #login-box button'        : 'submitLoginForm'
        },

        render: function() {
            var that = this;

            // Preload resources
            $.preload(
                this.getPreloadImages(),
                {
                    onFinish: function() {
                        $('#loading-text').fadeOut('slow', function() {
                            var context = {
                                    users: that.collection.toJSON()
                                },
                                content = Handlebars.compile(loginTemplate),
                                additionalContent = $('#required').html();

                            that.$el.html(content(context));

                            if (avrora.config.get('application.initialSetup')) {
                                avrora.config.set('application.initialSetup', false);
                                that.showUsers();
                            } else {
                                $('#login-container')
                                    .hide()
                                    .prepend(additionalContent)
                                    .transition({ scale : 0.1, opacity : 0 }, 5, function() {
                                        $(this).show();

                                        $(this).transition({ scale : 1, opacity : 1 }, 800, function() {
                                            that.showUsers();
                                            $('#login-container').find('.toolbar').fadeIn();
                                            that.showNotifications();
                                        });
                                    });
                            }
                        });
                    }
                }
            );

            return this;
        },

        /* View events */

        openLoginBox: function(e) {
            e.preventDefault();
            var $target = $(e.target),
                that = this;

            $target.off('openLogin');
            $target.on('openLogin', function() {
                var user = that.getSelectedUser();
                if (!user.get('password')) $('input[type=password]').hide();
                else $('input[type=password]').show();

                $target.stop().transition({ y: '-45px', height: '100px', width: '280px', scale: 1.0 }, function() {
                    $('#login-box').show().transition({ opacity: 1, top: '0px' }, function() {
                        $('input[type=password]').focus();

                        $($target.attr('href')).fadeIn('fast', function() {
                            $('.user-name').find('span').html($target.attr('data-title')).parent().fadeIn();
                            $('.close-btn').fadeIn();
                        });
                    });

                    $target.removeClass('loading');
                });
            });
            $target.off('closeLogin');
            $target.on('closeLogin', function() {
                $($target.attr('href')).fadeOut('fast');
                $target.removeClass('opened');
                $('.error').removeClass('error');
            });

            if (!$target.hasClass('opened') && !$target.parent().find('.opened').length) {
                $target.addClass('opened').addClass('loading');

                var otherAvatars = $target.parent().find('a.avatar:not([class*="opened"])');
                if (otherAvatars.length) {
                    otherAvatars.stop().transition({ scale: 0.5, opacity: 0 }, 200, 'snap', function() {
                        $(this).hide();

                        $target.trigger('openLogin');
                    });
                } else {
                    $target.trigger('openLogin');
                }
            } else if (!$target.hasClass('loading') && !$target.parent().find('.loading').length) {
                $('#login-box').transition({ opacity: 0, top: '-100px' }, function() {
                    $('.user-name, .close-btn').fadeOut('fast', function() {
                        $(this).find('span').html('');

                        $('#login-box').hide();
                        $target.parent().find('a.avatar:not([class*="opened"])').show();

                        $target.stop().transition({ y: '0px', height: '140px', width: '140px', scale: 1.0 }, function() {
                            var $otherAvatars = $(this).parent().find('a.avatar:not([class*="opened"])');

                            if ($otherAvatars.length) {
                                $otherAvatars.stop().transition({ scale: 1, opacity: 1 }, 300, 'in-out', function() {
                                    $target.trigger('closeLogin');
                                });
                            } else {
                                $target.trigger('closeLogin');
                            }
                        });
                    });
                });
            }
        },

        closeLoginBox: function(e) {
            e.preventDefault();

            $('#avatars').find('a.avatar.opened').click();
        },

        submitLoginForm: function(e) {
            e.preventDefault();

            var $passwordField = $(e.target).parents('#login-box').find('input[type=password]');
            $passwordField.removeClass('error');
            $('#login-box').removeClass('error');

            var passwordFieldValue = $passwordField.val(),
                user = this.getSelectedUser(),
                passwordHash = avrora.services.hashers.sha1(passwordFieldValue);

            if (user.get('password') !== '' && (passwordFieldValue === '' || user.get('password') !== passwordHash)) {
                $passwordField.addClass('error').focus();
                $('#login-box').addClass('error');
            } else {
                if (!avrora.config.get('application.debug')) {
                    var docElm = document.documentElement;
                    if (docElm.requestFullscreen) {
                        docElm.requestFullscreen();
                    } else if (docElm.mozRequestFullScreen) {
                        docElm.mozRequestFullScreen();
                    } else if (docElm.webkitRequestFullScreen) {
                        docElm.webkitRequestFullScreen();
                    }
                }

                user.signIn();

                $('#avatars, #login-box, .avrora-logo').transition({ scale: 0.45, opacity: 0 }, 500, function() {
                    $('#users').hide(function() {
                        avrora.router.navigate('#/desktop', {trigger: true});
                    });
                });
            }
        },

        /* View methods */

        getPreloadImages: function() {
            // Add files which need preloading
            var preloadImages = [];
            _.each(this.collection.models, function(user) {
                if (!_.isUndefined(user.get('avatar'))) preloadImages.push(user.get('avatar'));
                if (!_.isUndefined(user.get('cover'))) preloadImages.push(user.get('cover'));
            });

            return preloadImages;
        },

        showUsers: function() {
            var that = this;

            this.$el.find('#users').fadeIn(400, function() {
                that.$el.find('#login-box').hide().css({ 'opacity' : '0' }).css('top', '-100px');
                that.$el.find('.cover').hide().css('display', 'static');
            });
        },

        showNotifications: function() {
            this.$el.find('.notification').each( function() {
                var $noty = $(this);
                $noty.fadeIn(500, function() {
                    setTimeout(function() {
                        $noty.fadeOut();
                    }, 3000);
                });
            });
        },

        getSelectedUser: function() {
            var userId = $('#avatars').find('a.avatar.opened').first().attr('href').match(/[0-9]+/i)[0] * 1,
                user = this.collection.get(this.collection.url + userId + '/');

            return user;
        }
    });

    return avrora.namespace.views.LoginView;
});
