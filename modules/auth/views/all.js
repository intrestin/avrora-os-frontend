/**
 * Avrora OS autoloader for the module's views
 */

define([
	'underscore',

	// Module views
	'modules/auth/views/login',
	'modules/auth/views/welcome'
],

function(_, LoginView, WelcomeView) {

	var views = {
		login: LoginView,
		welcome: WelcomeView
	};

	return views;
});