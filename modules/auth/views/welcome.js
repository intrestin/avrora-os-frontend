define([
    'jquery',
    'underscore',
    'backbone',
    'handlebars',
    'plugins/jquery.transit',

    'avrora',

    // Templates
    'text!modules/auth/templates/welcome.html'
],

function($, _, Backbone, Handlebars, transit, avrora, welcomeTemplate) {

    avrora.namespace.views.WelcomeView = Backbone.View.extend({
        el: $('body'),

        events: {
            'click #create-user-form button' : 'submitCreateUser'
        },

        initialize: function() {
            var that = this;
            new avrora.services.inputs(this);

            this.on('formError', function(form, errors) { that.triggerErrors(form, errors); });

            this.collection.on('add', function() {
                that.collection.off('add');

                that.collection.fetch({
                    success : function() {
                        $('#window-box').transition({ scale : 0.1, opacity : 0 }, 500, function() {
                            $(this).remove();

                            $('#loading-text').fadeIn('slow', function() {
                                avrora.config.set('application.initialSetup', true);
                                avrora.router.navigate('#/login', {trigger: true});
                            });
                        });
                    }
                });
            });
        },

        render: function() {
            var that = this;

            $('#loading-text').fadeOut('slow', function() {
                var context = {
                        username: avrora.account.get('user').first_name || avrora.account.get('user').username
                    },
                    content = Handlebars.compile(welcomeTemplate);
                that.$el.append(content(context));

                $('#welcome-container').transition({ scale : 0.1, opacity : 0 }, 5, function() {
                    $(this).show();

                    $(this).transition({ scale : 1, opacity : 1 }, 1000, function() {
                        that.changeBox(1);
                    });
                });
            });

            return this;
        },

        /* View events */

        submitCreateUser: function(e) {
            e.preventDefault();

            var form = $(e.target).parents('#create-user-form'),
                username = $(form).find('input[name=name]'),
                password = $(form).find('input[name=password]'),

                rules = {
                    'name' : 'required',
                    'password' : 'required|confirmed',
                    'password_confirmed' : 'required'
                };

            if (!this.Inputs.validate(rules)) {
                if (!$(form).find('.error').length) this.trigger('formError', form, this.Inputs.errors);
            } else {
                this.collection.createUser({
                    name: username.val().trim(),
                    password: password.val()
                });
            }
        },

        /* View methods */

        changeBox: function(currentTextId) {
            var that = this;
            currentTextId *= 1;

            var currentTextElement = $('#text-' + currentTextId);
            if (!currentTextElement.length) return false;
            var timeoutTime = currentTextElement.data('timeout-time'),
                callbackFunction = currentTextElement.data('callback');

            currentTextElement.fadeIn(500);

            setTimeout(function() {
                currentTextId++;
                var nextTextElement = $('#text-' + currentTextId);

                if (callbackFunction) {
                    that['_' + callbackFunction](currentTextId - 1);
                } else if (nextTextElement.length) {
                    currentTextElement.fadeOut('slow', function() {
                        that.changeBox(currentTextId);
                    });
                }

            }, timeoutTime*1000);
        },

        _createUserStep: function(currentTextId) {
            $('#window-box').transition({'min-height': '280px'});
            $('#create-user-form').transition({ scale : 0.1, opacity : 0 }, 5, function() {
                $(this).show();

                $('#text-' + currentTextId).transition({ 'padding-top' : '20px' }, 700, function() {
                    $(this).css('height', 'auto');
                    $('#create-user-form').transition({ scale : 1, opacity : 1 }, 500);
                });
            });
        },

        triggerErrors: function(form, errors) {
            var uniqueErrors = {},
                $errorBox = $(form).find('.error-row');
            $errorBox.html('');

            _.each(_.keys(errors), function(inputName) {
                var $selector = $(form).find('input[name=' + inputName + ']');

                if (!(errors[inputName] in uniqueErrors)) {
                    uniqueErrors[errors[inputName]] = true;
                    $errorBox.prepend('<span>' + errors[inputName] + '</span><br>').hide();
                    $errorBox.css('padding-bottom', '10px');
                }

                $selector.addClass('error');
            });

            $errorBox.fadeIn();
            setTimeout(function() {
                $(form).find('.error').removeClass('error');
                $errorBox.fadeOut('fast', function() {
                    $errorBox.html('');
                    $errorBox.css('padding-bottom', '0px');
                });
            }, 2000);
        }
    });

    return avrora.namespace.views.WelcomeView;
});
