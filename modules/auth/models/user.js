define([
	'backbone',

	'avrora'
],

function(Backbone, avrora) {

	// OS' account users
	avrora.namespace.models.AccountUser = Backbone.RelationalModel.extend({
		urlRoot: avrora.config.get('application.api') + '/account-user/',
        relations: [
            {
                type: 'HasMany',
                key: 'applications',
                relatedModel: 'models.Application',
                collectionType: 'collections.Applications'
            }
        ],

        signIn: function() {
            avrora.account.currentUser = this;
            avrora.services.storage.setItem('currentUser', this);
        },

        signOut: function() {
            delete avrora.account.currentUser;
            avrora.services.storage.removeItem('currentUser');
        },

        hasInstalledApp: function(app) {
            var userApps = this.get('applications'),
                appUri = ('resource_uri' in app) ? app.resource_uri : app.url(),
                installedApp = this.getUserAppBy('uri', appUri);

            return installedApp;
        },

        installApplication: function(app) {
            this.get('applications').add(app);
            this.save();
        },

        uninstallApplication: function(app) {
            this.get('applications').remove(app);
            this.save();
        },

        openApplication: function(data, builtin) {
            if (!builtin) {
                var app = this.getUserAppBy('uri', data);
                if (!app) app = this.getUserAppBy('url', data);
                if (!app) return false;

                avrora.modules.get('desktop.views.applicationmanager').trigger('openApplication', app);
            } else {
                avrora.applications.openByUrl(data);
            }
        },

        getUserAppBy: function(type, data) {
            var application,
                userApps = this.get('applications');

            switch(type) {
            case 'uri':
                application = userApps.where({ resource_uri : data });
                break;
            case 'url':
                application = userApps.where({ url : data });
                break;
            }

            if (application.length) {
                return application[0];
            }

            return false;
        }
	});

	return avrora.namespace.models.AccountUser;
});