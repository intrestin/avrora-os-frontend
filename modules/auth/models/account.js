define([
    'backbone',

    'avrora'
],

function(Backbone, avrora) {

    // Account model - OS' main account
    avrora.namespace.models.Account = Backbone.RelationalModel.extend({
        urlRoot: avrora.config.get('application.api') + '/account/',
        relations: [
            {
                type: 'HasMany',
                key: 'users',
                relatedModel: 'models.AccountUser',
                collectionType: 'collections.AccountUsers'
            }
        ]
    });

    return avrora.namespace.models.Account;
});