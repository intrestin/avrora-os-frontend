/**
 * Avrora OS autoloader for the module's models
 */

define([
	'underscore',

	// Module models
	'modules/auth/models/account',
    'modules/auth/models/user'
],

function(_, Account, AccountUser) {

	var models = {
		account: Account,
		user: AccountUser
	};

	return models;
});