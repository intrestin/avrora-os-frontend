/**
 * Avrora OS starting file
 */

requirejs.config({
    paths: {
        // Application folders
        config: './config',
        modules: './modules',
        applications: './modules/applications',
        services: './modules/services',

        // JavaScript asset folders
        libs: '../assets/javascripts/libs',
        plugins: '../assets/javascripts/plugins',

        // JavaScript libraries
        underscore: '../assets/javascripts/libs/underscore',
        backbone: '../assets/javascripts/libs/backbone',
        handlebars: '../assets/javascripts/libs/handlebars',
        text: '../assets/javascripts/libs/text',
        tastypie: '../assets/javascripts/libs/backbone.tastypie',
        relational: '../assets/javascripts/libs/backbone.relational'
    },

    shim: {
        // What is which?
        jquery: {
            exports: '$'
        },
        underscore: {
            exports: '_'
        },
        handlebars: {
            exports: 'Handlebars'
        },
        backbone: {
            deps: ['underscore', 'jquery'],
            exports: 'Backbone'
        },
        tastypie: {
            deps: ['backbone']
        },
        relational: {
            deps: ['backbone']
        },
        avrora: {
            deps: ['backbone', 'tastypie', 'relational', 'handlebars']
        },

        // Which needs what?
        'plugins/jquery.preload': ['jquery'],
        'plugins/jquery.sortable': ['jquery'],
        'plugins/jquery.transit': ['jquery'],
        'plugins/jquery.mousewheel' : ['jquery'],
        'plugins/jquery.tooltipster' : ['jquery'],
        'plugins/sha1': ['jquery'],
        'plugins/md5': ['jquery'],
        'plugins/moment' : ['jquery'],
        'plugins/mousetrap' : ['jquery'],
        'plugins/fastclick' : ['jquery'],
        'plugins/html2canvas': ['jquery'],
        'plugins/jquery.select': ['jquery'],
        'plugins/dropzone-amd-module': ['jquery']
    }
});

require([
    'jquery',
    'router'
],

function($, Router) {
    // Let's get the party started
    $(function() {
        Router.initialize();
    });
});