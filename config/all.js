/**
 * Avrora OS autoloader for the config files
 */

define([
    'underscore',
    'config/application',
    'config/assets',
    'config/messages',
    'config/errors',
    'config/taskbar',
    'config/keybindings'
],

function(_, applicationConfig, assetsConfig, messagesConfig, errorsConfig, taskbarItemsConfig, keybindingsConfig) {
    // Get all config keys
    var configKeys = {
        application: applicationConfig,
        assets: assetsConfig,
        messages: messagesConfig,
        errors: errorsConfig,
        taskbar: taskbarItemsConfig,
        keybindings: keybindingsConfig
    };

    var configMethods = {
        /**
         * Determine if a configuration item or file exists.
         *
         * @param  string  key
         * @return bool
         */
        has: function(key) {
            return !!this.get(key);
        },

        /**
         * Get a configuration item.
         *
         * If no item is requested, the function will return false.
         *
         * @param  string  key
         * @return array, bool
         */
        get: function(key) {
            if (_.isUndefined(key)) return false;

            var keys = key.split('.');

            if (keys.length > 1) {
                var settingKey = configKeys;
                _.each(keys, function(key) {
                    settingKey = (_.has(settingKey, key)) ? settingKey[key] : false;
                });

                return settingKey;
            }

            return (_.has(configKeys, key)) ? configKeys[key] : false;
        },

        /**
         * Set a configuration item's value.
         *
         * @param  string  key
         * @param  mixed   value
         * @return void
         */
        set: function(key, value) {
            if (_.isUndefined(key)) return false;

            var keys = key.split('.');

            if (keys.length > 1) {
                try {
                    eval('configKeys["' + keys.join('"]["') + '"] = "' + JSON.stringify(value) + '";');
                } catch(error) {}
            } else {
                if (!_.has(configKeys, key)) {
                    configKeys[key] = value;
                }
            }
        }
    };

    return _.extend(configMethods, configKeys);
});