define({
    /*
    |--------------------------------------------------------------------------
    | A message shown when the user has gone offline
    |--------------------------------------------------------------------------
    */

    offline: {
        type: 'error',
        title: 'You are currently offline',
        content: 'Seems like you have gone offline. ' +
                'Avrora is switching to offline mode.<br>' +
                '<br>Internet connection is required for Avrora.',
        button: 'Continue using Avrora',
        speech: 'продължи',
        id: 'message-offline',
        inTaskbar: true,
        persistent: true
    },

    /*
    |--------------------------------------------------------------------------
    | Message is triggered when there is a speech error
    |--------------------------------------------------------------------------
    */

    speechError: {
        type: 'error',
        title: 'Speech error occured',
        content: 'There was a speech service error. ' +
                'Please try again or report this error.<br>' +
                '<br> Thank you for your patience!',
        button: 'Try again',
        speech: 'опитай отново',
        id: 'message-speech-error',
        inTaskbar: true,
        persistent: false
    },

    /*
    |--------------------------------------------------------------------------
    | Message triggered when the system can't recognize the speech input
    |--------------------------------------------------------------------------
    */

    speechNotRecognized: {
        type: 'error',
        title: 'Speech input not recognized',
        content: 'Sorry, I was unable to recognize your speech input. ' +
                'Could you please try again and try to speak more clearly ' +
                'and slowly. I am not that smart.' +
                '<br><br>Thank you!',
        button: 'Try again',
        speech: 'опитай отново',
        id: 'message-speech-not-recognized',
        inTaskbar: false,
        persistent: false
    }
});
