define({
    /*
    |--------------------------------------------------------------------------
    | Errors for form validation
    |--------------------------------------------------------------------------
    */

    'required' : 'All fields are required.',
    'required_singular' : 'This field is required.',

    'password_symbols' : 'The password must be at least %n% symbols.',
    'password_confirmed' : 'Passwords don\'t match.',

    'email' : 'The email address is not valid.'
});
