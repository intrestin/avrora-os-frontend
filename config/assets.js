define({
	/*
	|--------------------------------------------------------------------------
	| Default wallpapper URL
	|--------------------------------------------------------------------------
	*/

	defaultWallpaper: '/my/assets/images/wallpapers/default-wallpaper.jpg',

	/*
	|--------------------------------------------------------------------------
	| Default cover URL
	|--------------------------------------------------------------------------
	*/

	defaultCover: '/my/assets/images/wallpapers/default-wallpaper-cover.jpg',

	/*
	|--------------------------------------------------------------------------
	| Default avatar URL
	|--------------------------------------------------------------------------
	*/

	defaultAvatar: '/my/assets/images/no-avatar.gif',

	/*
	|--------------------------------------------------------------------------
	| Internet wallpapers API URL
	|--------------------------------------------------------------------------
	*/

	internetWallpapersApi: 'https://api.desktoppr.co/1/wallpapers'
});