define({
    /*
    |--------------------------------------------------------------------------
    | Shortcut keys
    |--------------------------------------------------------------------------
    */

    'ctrl+shift' : 'switchBetweenApps',
    'ctrl+alt+left' : 'resizeLeft',
    'ctrl+alt+right' : 'resizeRight',
    'ctrl+alt+down' : 'resizeDown',
    'ctrl+alt+up' : 'resizeUp',
    'ctrl+alt+f' : 'switchFullscreen',
    'ctrl+q' : 'quitApplication',
    'ctrl+l' : 'initSpeech',
    'ctrl+alt+d' : 'showDesktop',
    'ctrl+a' : 'openAssistant'
});
