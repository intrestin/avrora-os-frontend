define({
	/*
	|--------------------------------------------------------------------------
	| Debug
	|--------------------------------------------------------------------------
	|
	| Enable or not debugging version of the avrora OS. With debug set to true 
	| you can see logs in the console.
	|
	*/

	debug: true,

	/*
	|--------------------------------------------------------------------------
	| Base URL
	|--------------------------------------------------------------------------
	|
	| The application's base URL
	|
	*/

	baseURL: 'http://127.0.0.1:8000/my',

	/*
	|--------------------------------------------------------------------------
	| API URL
	|--------------------------------------------------------------------------
	|
	| The URL used to access Avrora's application programming interface (API).
	| The API is the brain of the OS. The URL should not end with a trailing
	| slash.
	|
	*/

	api: '/api/v1',

	/*
	|--------------------------------------------------------------------------
	| Installed modules
	|--------------------------------------------------------------------------
	|
	| A list of installed modules.
	|
	*/

	installedModules: [
		'auth',
		'desktop'
	],

	/*
	|--------------------------------------------------------------------------
	| Built-in applications
	|--------------------------------------------------------------------------
	|
	| A list of integrated user applications.
	|
	*/

	builtinApplications: [
		'settings',
		'market',
		'assistant'
	],

	/*
	|--------------------------------------------------------------------------
	| Disable context menu
	|--------------------------------------------------------------------------
	|
	| Whether or not to disable the context menu, triggered by right clicking
	| on the page.
	|
	*/

	disableContextMenu: false,

	/*
	|--------------------------------------------------------------------------
	| Enable application cache
	|--------------------------------------------------------------------------
	|
	| Enable application cache to take a use of the HTML5 cache. This is really
	| powerfull, especially for offline use.
	|
	*/

	enableApplicationCache: false,

	/*
	|--------------------------------------------------------------------------
	| Websocket retries
	|--------------------------------------------------------------------------
	|
	| The maximum number of times that a websocket request should retry if it
	| fails.
	|
	*/

	websocketRetries: 100,

	/*
	|--------------------------------------------------------------------------
	| Ping
	|--------------------------------------------------------------------------
	|
	| Enable or disable ping checks.
	|
	*/

	ping: true,

	/*
	|--------------------------------------------------------------------------
	| Ping delay
	|--------------------------------------------------------------------------
	|
	| Delay between ping checks - 30s default.
	|
	*/

	pingDelay: 10000
});